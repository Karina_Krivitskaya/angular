import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterBy'
})
export class FilterByPipe implements PipeTransform {

  transform(array: any[], args: any): any {
    if (args[0] && args[0]['name']) {
      return array.filter(item => item.name.toLowerCase().indexOf(args[0]['name'].toLowerCase()) !== -1);
    }

    return array;
  }
}

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatTime'
})
export class FormatTimePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (value < 60) {
      return value ? `${value}min` : '';
    }

    return value ? `${Math.floor(value / 60)}h ${value % 60}min` : '';
  }

}

import { FormatTimePipe } from './format-time.pipe';

describe('FormatTimePipe', () => {
  it('create an instance', () => {
    const pipe = new FormatTimePipe();
    expect(pipe).toBeTruthy();
  });

  it('transforms 100 to "1h 40min"', () => {
    const pipe = new FormatTimePipe();
    expect(pipe.transform(100)).toBe('1h 40min');
  });

  it('transforms null to ""', () => {
    const pipe = new FormatTimePipe();
    expect(pipe.transform(null)).toBe('');
  });
});

import { FilterByPipe } from './filter-by.pipe';

describe('FilterByPipe', () => {
  it('create an instance', () => {
    const pipe = new FilterByPipe();
    expect(pipe).toBeTruthy();
  });

  it('should correctly order by title', () => {
    const pipe = new FilterByPipe();
    const courses = [
      {name: 'abc', id: 1, length: 1, date: '01.01.2016', isTopRated: false, description: 'abc', author: []},
      {name: 'abcd', id: 2, length: 2, date: '01.01.2016', isTopRated: true, description: 'abcd', author: []},
      {name: 'abcde', id: 3, length: 3, date: '01.01.2016', isTopRated: true, description: 'abcde', author: []}
    ];
    expect(pipe.transform(courses, [{'name': 'abcd'}])).toEqual([
      {name: 'abcd', id: 2, length: 2, date: '01.01.2016', isTopRated: true, description: 'abcd', author: []},
      {name: 'abcde', id: 3, length: 3, date: '01.01.2016', isTopRated: true, description: 'abcde', author: []}
    ]);

    expect(pipe.transform(courses, [{'name': 'abcd'}])).toEqual([
      {name: 'abcd', id: 2, length: 2, date: '01.01.2016', isTopRated: true, description: 'abcd', author: []},
      {name: 'abcde', id: 3, length: 3, date: '01.01.2016', isTopRated: true, description: 'abcde', author: []}
    ]);
  });

  it('should return initial array if it is not title', () => {
    const pipe = new FilterByPipe();
    const courses = [
      {name: 'abc', id: 1, length: 1, date: '01.01.2016', isTopRated: false, description: 'abc', author: []},
      {name: 'abcd', id: 2, length: 2, date: '01.01.2016', isTopRated: true, description: 'abcd', author: []},
      {name: 'abcde', id: 3, length: 3, date: '01.01.2016', isTopRated: true, description: 'abcde', author: []}
    ];
    expect(pipe.transform(courses, [])).toEqual([
      {name: 'abc', id: 1, length: 1, date: '01.01.2016', isTopRated: false, description: 'abc', author: []},
      {name: 'abcd', id: 2, length: 2, date: '01.01.2016', isTopRated: true, description: 'abcd', author: []},
      {name: 'abcde', id: 3, length: 3, date: '01.01.2016', isTopRated: true, description: 'abcde', author: []}
    ]);
  });
});

import { OrderByPipe } from './order-by.pipe';

describe('OrderByPipe', () => {
  it('create an instance', () => {
    const pipe = new OrderByPipe();
    expect(pipe).toBeTruthy();
  });

  it('should correctly order by date', () => {
    const pipe = new OrderByPipe();
    const courses = [
      {name: 'a', id: 1, duration: 1, date: new Date(2016, 1, 1), isTopRated: false, description: 'abc'},
      {name: 'b', id: 2, duration: 2, date: new Date(2018, 2, 2), isTopRated: true, description: 'abcd'},
      {name: 'c', id: 3, duration: 3, date: new Date(2016, 1, 1), isTopRated: true, description: 'abcde'}
    ];
    expect(pipe.transform(courses, 'date')).toEqual([
      {name: 'a', id: 1, duration: 1, date: new Date(2016, 1, 1), isTopRated: false, description: 'abc'},
      {name: 'c', id: 3, duration: 3, date: new Date(2016, 1, 1), isTopRated: true, description: 'abcde'},
      {name: 'b', id: 2, duration: 2, date: new Date(2018, 2, 2), isTopRated: true, description: 'abcd'}
    ]);
  });

  it('should correctly order by id', () => {
    const pipe = new OrderByPipe();
    const courses = [
      {name: 'a', id: 1, duration: 1, date: new Date(2016, 1, 1), isTopRated: false, description: 'abc'},
      {name: 'c', id: 3, duration: 3, date: new Date(2016, 1, 1), isTopRated: true, description: 'abcde'},
      {name: 'b', id: 2, duration: 2, date: new Date(2018, 2, 2), isTopRated: true, description: 'abcd'}
    ];
    expect(pipe.transform(courses, 'id')).toEqual([
      {name: 'a', id: 1, duration: 1, date: new Date(2016, 1, 1), isTopRated: false, description: 'abc'},
      {name: 'b', id: 2, duration: 2, date: new Date(2018, 2, 2), isTopRated: true, description: 'abcd'},
      {name: 'c', id: 3, duration: 3, date: new Date(2016, 1, 1), isTopRated: true, description: 'abcde'}
    ]);
  });
});

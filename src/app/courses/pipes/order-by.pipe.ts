import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'orderBy'
})
export class OrderByPipe implements PipeTransform {

  transform(array: any[], args: any): any {
    array.sort((a: any, b: any) => {
      const date1 = new Date(a[args]);
      const date2 = new Date(b[args]);
      const first = args === 'date' ? date1.getTime() : a[args];
      const second = args === 'date' ? date2.getTime() : b[args];
      if (first < second) {
        return -1;
      } else if (first > second) {
        return 1;
      } else {
        return 0;
      }
    });

    return array;
  }

}

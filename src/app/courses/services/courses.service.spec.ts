import { CoursesService } from './courses.service';
import {CourseItem} from '../models/course-item.model';
import {LoadingService} from '../../core/services/loading/loading.service';

describe('CoursesService', () => {
  let coursesService;
  let httpClientSpy: { get: jasmine.Spy };

  beforeEach(() => {
    const spy = jasmine.createSpyObj('LoadingService', ['activate']);
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'patch', 'post', 'delete']);
    coursesService = new CoursesService(<any> httpClientSpy, spy);
  });

  it('should be created', () => {
    expect(coursesService).toBeTruthy();
  });

  it('getCoursesList method should return courses list', () => {
    coursesService.getCoursesList('0', '5');
    expect(coursesService.http.get).toHaveBeenCalledWith('http://localhost:3004/courses', {params: {start: '0', count: '5'}});
  });

  it('searchVideo method should search video', () => {
    coursesService.searchVideo('abc');
    expect(coursesService.http.get).toHaveBeenCalledWith('http://localhost:3004/courses', {params: {textFragment: 'abc'}});
  });

  it('createCourse method should create course', () => {
    const course = new CourseItem({
      name: 'title',
      id: 1,
      length: 10,
      date: '01.01.2017',
      isTopRated: false,
      description: '',
      authors: []
    });
    coursesService.createCourse(course);
    expect(coursesService.http.post).toHaveBeenCalledWith('http://localhost:3004/courses', course);
  });

  it('getCourseById method should get course by id', () => {
    coursesService.getCourseById(1);
    expect(coursesService.http.get).toHaveBeenCalledWith('http://localhost:3004/courses/1');
  });

  it('getAuthors method should update course', () => {
    coursesService.getAuthors();
    expect(coursesService.http.get).toHaveBeenCalledWith('http://localhost:3004/authors');
  });

  it('updateCourse method should update course', () => {
    coursesService.updateCourse({id: 1});
    expect(coursesService.http.patch).toHaveBeenCalledWith('http://localhost:3004/courses/1', {id: 1});
  });

  it('removeCourse method should remove course if confirm return false', () => {
    spyOn(window, 'confirm').and.callFake(function () {
      return false;
    });
    coursesService.removeCourse(1);
    expect(coursesService.http.delete).not.toHaveBeenCalled();
  });

  it('removeCourse method should remove course if confirm return true', () => {
    spyOn(window, 'confirm').and.callFake(function () {
      return true;
    });
    coursesService.removeCourse(1);
    expect(coursesService.http.delete).toHaveBeenCalledWith('http://localhost:3004/courses/1');
  });
});

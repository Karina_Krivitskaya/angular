import { Injectable } from '@angular/core';
import {CourseItem} from '../models/course-item.model';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {LoadingService} from '../../core/services/loading/loading.service';
import {AuthorModel} from '../models/author.model';

const BASE_URL = 'http://localhost:3004';

@Injectable()
export class CoursesService {
  constructor(private http: HttpClient, private loadingService: LoadingService) { }

  public getCoursesList(start: string, count: string): Observable<CourseItem[]> {
    return this.http.get<CourseItem[]>(`${BASE_URL}/courses`, {params: {start, count}});
  }

  public createCourse(course: CourseItem) {
    return this.http.post<CourseItem>(`${BASE_URL}/courses`, course);
  }

  public getCourseById(id: number): Observable<CourseItem> {
    return this.http.get<CourseItem>(`${BASE_URL}/courses/${id}`);
  }

  public getAuthors(): Observable<AuthorModel[]> {
    return this.http.get<AuthorModel[]>(`${BASE_URL}/authors`);
  }

  public searchVideo(textFragment: string) {
    return this.http.get<CourseItem[]>(`${BASE_URL}/courses`, {params: {textFragment}});
  }

  public updateCourse(course: CourseItem) {
    return this.http.patch(`${BASE_URL}/courses/${course.id}`, course);
  }

  public removeCourse(id: number): Observable<CourseItem> {
    let confirmDelete;
    confirmDelete = confirm('Do you really want to delete this course?');
    if (confirmDelete) {
      this.loadingService.activate();
      return this.http.delete<CourseItem>(`${BASE_URL}/courses/${id}`);
    }
  }
}

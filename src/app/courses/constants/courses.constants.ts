export const COUNT_ITEM_REQUEST = 10;
export const COUNT_ITEM_LOAD = 5;

export const DATE_FORMAT = /[0-9]{2}\.[0-9]{2}\.[0-9]{4}$/;
export const NUMBER = /^\d+$/;

import { SelectCourseDirective } from './select-course.directive';
import {TestBed, ComponentFixture} from '@angular/core/testing';
import {Component, DebugElement} from '@angular/core';
import {By} from '@angular/platform-browser';

@Component({
  template: `<div class="after" [appSelectCourse]="dateAfter"></div><div class="initial" [appSelectCourse]="date"></div><div class="before" [appSelectCourse]="date10DaysAgo"></div>`
})
class TestComponent {
  public dateAfter = new Date(2020, 10, 10);
  public date10DaysAgo = new Date();
  public date = new Date(2018, 2, 20);
}

describe('SelectCourseDirective', () => {
  let component: TestComponent;
  let fixture: ComponentFixture<TestComponent>;
  let afterEl: DebugElement;
  let beforeEl: DebugElement;
  let el: DebugElement;

  beforeEach(() => {
    fixture = TestBed.configureTestingModule({
      declarations: [ SelectCourseDirective, TestComponent ]
    })
    .createComponent(TestComponent);

    component = fixture.componentInstance;
    afterEl = fixture.debugElement.query(By.css('.after'));
    beforeEl = fixture.debugElement.query(By.css('.before'));
    el = fixture.debugElement.query(By.css('.initial'));
    component.date10DaysAgo.setDate(component.date10DaysAgo.getDate() - 10);
  });

  it('should have one green div and one blue div', () => {
    fixture.detectChanges();
    expect(beforeEl.nativeElement.style.boxShadow).toBe('green 0px 0px 20px');
    expect(afterEl.nativeElement.style.boxShadow).toBe('rgb(0, 31, 128) 0px 0px 20px');
  });

  it('should not change border', () => {
    fixture.detectChanges();
    expect(el.nativeElement.style.boxShadow).toBe('');
  });
});

import {Directive, ElementRef, Input} from '@angular/core';

@Directive({
  selector: '[appSelectCourse]'
})
export class SelectCourseDirective {
  @Input() set appSelectCourse(videoCourse: any) {
    const videoCourseDate = new Date(videoCourse);
    const currentDate = new Date();
    const date14DaysAgo = new Date();
    date14DaysAgo.setDate(currentDate.getDate() - 14);

    if (videoCourseDate.getTime() < currentDate.getTime() &&
      videoCourseDate.getTime() >= date14DaysAgo.getTime()) {
      this.element.nativeElement.style.boxShadow = '0 0 20px green';
    } else if (videoCourseDate.getTime() > currentDate.getTime()) {
      this.element.nativeElement.style.boxShadow = '0 0 20px #001f80';
    }
  }

  constructor(public element: ElementRef) { }

}

import {Injectable} from '@angular/core';
import {Action, Store} from '@ngrx/store';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {catchError, map, switchMap, tap, withLatestFrom} from 'rxjs/internal/operators';
import {Observable, of} from 'rxjs';
import {Router} from '@angular/router';
import {CoursesService} from '../services/courses.service';
import {LoadingService} from '../../core/services/loading/loading.service';
import {
  AddCourseAction, CoursesActionTypes, EditCourseAction, GetAuthorsSuccessAction, GetCourseByIDAction, GetCourseByIDFailureAction,
  GetCourseByIDSuccessAction,
  GetCoursesAction,
  GetCoursesSuccessAction,
  InitialStateAction,
  RemoveCourseAction,
  RemoveCourseSuccessAction, SearchAction, SearchSuccessAction,
  UpdateCoursesListAction,
  UpdateCoursesListSuccessAction
} from '../actions/courses.action';

@Injectable()
export class CoursesEffects {

  constructor(
    private store: Store<any>,
    private actions: Actions,
    private coursesService: CoursesService,
    private router: Router,
    private loadingService: LoadingService
  ) {  }

  @Effect()
  getCourses: Observable<Action> = this.actions.pipe(
    ofType(CoursesActionTypes.GET_COURSES),
    map((action: GetCoursesAction) => action.payload),
    switchMap(payload => {
      return this.coursesService.getCoursesList(payload.start, payload.count).pipe(
        map(courses => {
          return new GetCoursesSuccessAction({ coursesList: courses });
        }));
    })
  );

  @Effect({ dispatch: false })
  getCoursesSuccess: Observable<any> = this.actions.pipe(
    ofType(CoursesActionTypes.GET_COURSES_SUCCESS),
    tap(() => {
      this.loadingService.deactivate();
    })
  );

  @Effect()
  getCourseByID: Observable<Action> = this.actions.pipe(
    ofType(CoursesActionTypes.GET_COURSE_BY_ID),
    map((action: GetCourseByIDAction) => action.payload),
    switchMap(payload => {
      return this.coursesService.getCourseById(payload.id).pipe(
        map(course => new GetCourseByIDSuccessAction({ course: course })),
        catchError(() => of(new GetCourseByIDFailureAction))
      );
    })
  );

  @Effect({ dispatch: false })
  getCourseByIDSuccess: Observable<any> = this.actions.pipe(
    ofType(CoursesActionTypes.GET_COURSE_BY_ID_SUCCESS),
    tap(() => {
      this.loadingService.deactivate();
    })
  );

  @Effect({ dispatch: false })
  getCourseByIDFailure: Observable<any> = this.actions.pipe(
    ofType(CoursesActionTypes.GET_COURSE_BY_ID_FAILURE),
    tap(() => {
      this.router.navigate(['/error']);
      this.loadingService.deactivate();
    })
  );

  @Effect()
  getAuthors: Observable<Action> = this.actions.pipe(
    ofType(CoursesActionTypes.GET_AUTHORS),
    switchMap(() => {
      return this.coursesService.getAuthors().pipe(
        map(authors => new GetAuthorsSuccessAction({ authors: authors }))
      );
    })
  );

  @Effect()
  updateCourses: Observable<Action> = this.actions.pipe(
    ofType(CoursesActionTypes.UPDATE_COURSES_LIST),
    map((action: GetCoursesAction) => action.payload),
    switchMap(payload => {
      return this.coursesService.getCoursesList('0', payload.count.toString()).pipe(
        map(courses => {
          return new UpdateCoursesListSuccessAction({ coursesList: courses });
        }));
    })
  );

  @Effect({ dispatch: false })
  updateCoursesSuccess: Observable<any> = this.actions.pipe(
    ofType(CoursesActionTypes.UPDATE_COURSES_LIST_SUCCESS),
    tap(() => {
      this.loadingService.deactivate();
    })
  );

  @Effect({ dispatch: false })
  addCourse: Observable<any> = this.actions.pipe(
    ofType(CoursesActionTypes.ADD_COURSE),
    map((action: AddCourseAction) => action.payload),
    switchMap(payload => {
      return this.coursesService.createCourse(payload.course).pipe(
        tap(() => {
          this.router.navigate(['/courses']);
          this.loadingService.deactivate();
        })
      );
    })
  );

  @Effect({ dispatch: false })
  editCourse: Observable<any> = this.actions.pipe(
    ofType(CoursesActionTypes.EDIT_COURSE),
    map((action: EditCourseAction) => action.payload),
    switchMap(payload => {
      return this.coursesService.updateCourse(payload.course).pipe(
        tap((course) => {
          this.router.navigate(['/courses']);
          this.loadingService.deactivate();
        })
      );
    })
  );

  @Effect()
  searchCourses: Observable<Action> = this.actions.pipe(
    ofType(CoursesActionTypes.SEARCH),
    map((action: SearchAction) => action.payload),
    switchMap(payload => {
      return this.coursesService.searchVideo(payload.video).pipe(
        map((courses) => {
          return new SearchSuccessAction({ coursesList: courses });
        }));
    })
  );

  @Effect({ dispatch: false })
  searchSuccess: Observable<any> = this.actions.pipe(
    ofType(CoursesActionTypes.SEARCH_SUCCESSS),
    tap(() => {
      this.loadingService.deactivate();
    })
  );

  @Effect()
  removeCourse: Observable<Action> = this.actions.pipe(
    ofType(CoursesActionTypes.REMOVE_COURSE),
    map((action: RemoveCourseAction) => action.payload),
    switchMap(payload => {
      return this.coursesService.removeCourse(payload.id).pipe(
        map(() => {
          return new RemoveCourseSuccessAction;
        }));
    })
  );

  @Effect({ dispatch: false })
  removeCoursesSuccess: Observable<any> = this.actions.pipe(
    ofType(CoursesActionTypes.REMOVE_COURSE_SUCCESS),
    withLatestFrom(this.store),
    tap(([action, state]) => {
      if (state.courses.searchName) {
        this.store.dispatch(new SearchAction({ video: state.courses.searchName }));
      } else {
        this.store.dispatch(new UpdateCoursesListAction({ count: state.courses.coursesList.length }));
      }
    })
  );

  @Effect({ dispatch: false })
  cancel: Observable<any> = this.actions.pipe(
    ofType(CoursesActionTypes.CANCEL),
    tap(() => {
      this.router.navigate(['/courses']);
      this.store.dispatch(new InitialStateAction);
    })
  );
}

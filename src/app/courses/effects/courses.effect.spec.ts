import {BehaviorSubject, Observable, of, throwError} from 'rxjs';
import {CoursesEffects} from './courses.effect';
import {TestBed} from '@angular/core/testing';
import {CoursesService} from '../services/courses.service';
import {LoadingService} from '../../core/services/loading/loading.service';
import {Action, Store, StoreModule} from '@ngrx/store';
import {Router} from '@angular/router';
import {provideMockActions} from '@ngrx/effects/testing';
import {
  AddCourseAction, CancelAction, EditCourseAction, GetAuthorsAction, GetAuthorsSuccessAction,
  GetCourseByIDAction, GetCourseByIDFailureAction, GetCourseByIDSuccessAction, GetCoursesAction,
  GetCoursesSuccessAction, InitialStateAction, RemoveCourseAction, RemoveCourseSuccessAction, SearchAction,
  SearchSuccessAction,
  UpdateCoursesListAction,
  UpdateCoursesListSuccessAction
} from '../actions/courses.action';
import {cold, hot} from 'jasmine-marbles';
import {coursesReducer} from '../reducers/courses.reducer';
import {map} from 'rxjs/internal/operators';
import {AppState} from '../../store/app.state';
import {CourseItem} from '../models/course-item.model';

export class TestStore extends BehaviorSubject<AppState> {
  constructor(private _initialState) {
    super(_initialState);
  }
  dispatch = (action: Action): void => {
  }

  select = (pathOrMapFn: any, ...paths: string[]): Observable<AppState> => {
    return map.call(this, pathOrMapFn);
  }
}

describe('CourseEffects', () => {
  let coursesEffects: CoursesEffects;
  let loadingService;
  let actions: Observable<any>;
  let coursesService;
  let router;
  let store;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        StoreModule.forRoot(coursesReducer),
      ],
      providers: [
        CoursesEffects,
        {
          provide: CoursesService,
          useValue: {
            getAuthors: () => of({}),
            getCoursesList: () => of({}),
            getCourseById: () => of({}),
            createCourse: () => of({}),
            searchVideo: () => of({}),
            updateCourse: () => of({}),
            removeCourse: () => of({}),
          }
        },
        {
          provide: LoadingService,
          useValue: {
            deactivate: () => {}
          }
        },
        {
          provide: Store,
          useValue: new TestStore({
            courses: {
              coursesList: [
                {
                  id: 1,
                  name: 'duis mollit reprehenderit ad',
                  description: 'abc',
                  isTopRated: false,
                  date: '2017-09-28T04:39:24+00:00',
                  authors: null,
                  length: 157
                },
                {
                  id: 2,
                  name: 'duis mollit reprehenderit ad',
                  description: 'abc',
                  isTopRated: false,
                  date: '2017-09-28T04:39:24+00:00',
                  authors: null,
                  length: 157
                }
              ],
              editedCourse: null,
              loadCourses: true,
              searchName: 'video',
              errorMessage: null
            }
          })
        },
        {
          provide: Router,
          useValue: {
            navigate: () => {}
          }
        },
        provideMockActions(() => actions)
      ]
    });
    coursesEffects = TestBed.get(CoursesEffects);
    router = TestBed.get(Router);
    store = TestBed.get(Store);
    coursesService = TestBed.get(CoursesService);
    loadingService = TestBed.get(LoadingService);

    spyOn(loadingService, 'deactivate');
    spyOn(store, 'dispatch');
    spyOn(router, 'navigate');
  });

  it('should be created', () => {
    expect(coursesEffects).toBeTruthy();
  });

  describe('getCourses', () => {
    it('should return an GetCoursesSuccess action on success', () => {
      const action = new GetCoursesAction({ start: '0', count: '10' });
      const completion = new GetCoursesSuccessAction({coursesList: []} );

      actions = hot('a', { a: action });
      const expected = cold('b', { b: completion });

      spyOn(coursesService, 'getCoursesList').and.returnValue(of([]));

      expect(coursesEffects.getCourses).toBeObservable(expected);
    });
  });

  describe('getCoursesSuccess', () => {
    it('should deactivate loading', () => {
      const action = new GetCoursesSuccessAction({ coursesList: [] });

      actions = of(action);
      coursesEffects.getCoursesSuccess.subscribe();

      expect(loadingService.deactivate).toHaveBeenCalled();
    });
  });

  describe('getCourseByID', () => {
    it('should return an GetCourseByIDSuccess action on success', () => {
      const course = {
        id: 1,
        name: 'duis mollit reprehenderit ad',
        description: 'abc',
        isTopRated: false,
        date: '2017-09-28T04:39:24+00:00',
        authors: null,
        length: 157
      };
      const action = new GetCourseByIDAction({id: 1});
      const completion = new GetCourseByIDSuccessAction({course: course} );

      actions = hot('a', { a: action });
      const expected = cold('b', { b: completion });

      spyOn(coursesService, 'getCourseById').and.returnValue(of(course));

      expect(coursesEffects.getCourseByID).toBeObservable(expected);
    });

    it('should return an GetCourseByIDFailureAction action, with an error, on failure', () => {
      const action = new GetCourseByIDAction({ id: 0 });
      const completion = new GetCourseByIDFailureAction;

      actions = hot('a', { a: action });
      const expected = cold('b', { b: completion });

      spyOn(coursesService, 'getCourseById').and.returnValue(throwError({}));

      expect(coursesEffects.getCourseByID).toBeObservable(expected);
    });
  });

  describe('getCourseByIDSuccess', () => {
    it('should deactivate loading', () => {
      const action = new GetCourseByIDSuccessAction({course: new CourseItem({
          name: '',
          id: 1,
          length: 1,
          date: '',
          isTopRated: false,
          description: '',
          authors: [],
        })
      });

      actions = of(action);
      coursesEffects.getCourseByIDSuccess.subscribe();

      expect(loadingService.deactivate).toHaveBeenCalled();
    });
  });

  describe('getCourseByIDFailure', () => {
    it('should deactivate loading and navigate to error page', () => {
      const action = new GetCourseByIDFailureAction;

      actions = of(action);
      coursesEffects.getCourseByIDFailure.subscribe();

      expect(loadingService.deactivate).toHaveBeenCalled();
      expect(router.navigate).toHaveBeenCalledWith(['/error']);
    });
  });

  describe('getAuthors', () => {
    it('should return an GetAuthorsSuccessAction action on success', () => {
      const action = new GetAuthorsAction;
      const completion = new GetAuthorsSuccessAction({authors: []} );

      actions = hot('a', { a: action });
      const expected = cold('b', { b: completion });

      spyOn(coursesService, 'getAuthors').and.returnValue(of([]));

      expect(coursesEffects.getAuthors).toBeObservable(expected);
    });
  });

  describe('updateCourses', () => {
    it('should return an UpdateCoursesListSuccess action on success', () => {
      const action = new UpdateCoursesListAction({ count: 10 });
      const completion = new UpdateCoursesListSuccessAction({coursesList: []} );

      actions = hot('a', { a: action });
      const expected = cold('b', { b: completion });

      spyOn(coursesService, 'getCoursesList').and.returnValue(of([]));

      expect(coursesEffects.updateCourses).toBeObservable(expected);
    });
  });

  describe('updateCoursesSuccess', () => {
    it('should deactivate loading', () => {
      const action = new UpdateCoursesListSuccessAction({ coursesList: [] });

      actions = of(action);
      coursesEffects.updateCoursesSuccess.subscribe();

      expect(loadingService.deactivate).toHaveBeenCalled();
    });
  });

  describe('addCourse', () => {
    it('should deactivate loading and navigate to courses page', () => {
      const action = new AddCourseAction({ course: new CourseItem({
          name: '',
          id: 1,
          length: 1,
          date: '',
          isTopRated: false,
          description: '',
          authors: [],
        })
      });

      actions = of(action);
      coursesEffects.addCourse.subscribe();

      expect(loadingService.deactivate).toHaveBeenCalled();
      expect(router.navigate).toHaveBeenCalledWith(['/courses']);
    });
  });

  describe('editCourse', () => {
    it('should deactivate loading and navigate to courses page', () => {
      const action = new EditCourseAction({ course: new CourseItem({
          name: '',
          id: 1,
          length: 1,
          date: '',
          isTopRated: false,
          description: '',
          authors: [],
        })
      });

      actions = of(action);
      coursesEffects.editCourse.subscribe();

      expect(loadingService.deactivate).toHaveBeenCalled();
      expect(router.navigate).toHaveBeenCalledWith(['/courses']);
    });
  });

  describe('searchCourses', () => {
    it('should return an SearchSuccess action on success', () => {
      const action = new SearchAction({ video: 'video' });
      const completion = new SearchSuccessAction({coursesList: []} );

      actions = hot('a', { a: action });
      const expected = cold('b', { b: completion });

      spyOn(coursesService, 'searchVideo').and.returnValue(of([]));

      expect(coursesEffects.searchCourses).toBeObservable(expected);
    });
  });

  describe('searchSuccess', () => {
    it('should deactivate loading', () => {
      const action = new SearchSuccessAction({ coursesList: [] });

      actions = of(action);
      coursesEffects.searchSuccess.subscribe();

      expect(loadingService.deactivate).toHaveBeenCalled();
    });
  });

  describe('removeCourse', () => {
    it('should return an SearchSuccess action on success', () => {
      const action = new RemoveCourseAction({ id: 1 });
      const completion = new RemoveCourseSuccessAction;

      actions = hot('a', { a: action });
      const expected = cold('b', { b: completion });

      spyOn(coursesService, 'removeCourse').and.returnValue(of({}));

      expect(coursesEffects.removeCourse).toBeObservable(expected);
    });
  });

  describe('cancel', () => {
    it('should navigate to courses page and dispatch new InitialStateAction', () => {
      const action = new CancelAction;

      actions = of(action);
      coursesEffects.cancel.subscribe();

      expect(router.navigate).toHaveBeenCalledWith(['/courses']);
      expect(store.dispatch).toHaveBeenCalledWith(new InitialStateAction);
    });
  });

  describe('removeCoursesSuccess', () => {
    it('should dispatch new SearchAction if list filtered', () => {
      const action = new RemoveCourseSuccessAction;


      actions = of(action);
      coursesEffects.removeCoursesSuccess.subscribe();

      expect(store.dispatch).toHaveBeenCalledWith(new SearchAction({ video: 'video' }));
    });

    it('should dispatch new UpdateCoursesList if list not filtered', () => {
      const action = new RemoveCourseSuccessAction;
      store.next({
        courses: {
          coursesList: [
            {
              id: 1,
              name: 'duis mollit reprehenderit ad',
              description: 'abc',
              isTopRated: false,
              date: '2017-09-28T04:39:24+00:00',
              authors: null,
              length: 157
            },
            {
              id: 2,
              name: 'duis mollit reprehenderit ad',
              description: 'abc',
              isTopRated: false,
              date: '2017-09-28T04:39:24+00:00',
              authors: null,
              length: 157
            }
          ],
          editedCourse: null,
          loadCourses: true,
          searchName: null,
          errorMessage: null
        }
      });

      actions = of(action);
      coursesEffects.removeCoursesSuccess.subscribe();

      expect(store.dispatch).toHaveBeenCalledWith(new UpdateCoursesListAction({ count: 2 }));
    });
  });
});

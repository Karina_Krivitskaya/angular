import {Injectable} from '@angular/core';
import {Action} from '@ngrx/store';
import {
  Course, CourseAuthors, CourseCountParams, CourseId, CourseParams, CoursesList,
  CourseVideo
} from '../models/course-item.model';

export enum CoursesActionTypes {
  GET_COURSES = '[COURSES] GET_COURSES',
  GET_COURSES_SUCCESS = '[COURSES] GET_COURSES_SUCCESS',
  GET_COURSE_BY_ID = '[COURSES] GET_COURSE_BY_ID',
  GET_COURSE_BY_ID_SUCCESS = '[COURSES] GET_COURSE_BY_ID_SUCCESS',
  GET_COURSE_BY_ID_FAILURE = '[COURSES] GET_COURSE_BY_ID_FAILURE',
  GET_AUTHORS = '[COURSES] GET_AUTHORS',
  GET_AUTHORS_SUCCESS = '[COURSES] GET_AUTHORS_SUCCESS',
  ADD_COURSE = '[COURSES] ADD_COURSE',
  EDIT_COURSE = '[COURSES] EDIT_COURSE',
  SEARCH = '[COURSES] SEARCH',
  SEARCH_SUCCESSS = '[COURSES] SEARCH_SUCCESSS',
  INITIAL_STATE = '[COURSES] INITIAL_STATE',
  REMOVE_COURSE = '[COURSES] REMOVE_COURSE',
  REMOVE_COURSE_SUCCESS = '[COURSES] REMOVE_COURSE_SUCCESS',
  UPDATE_COURSES_LIST = '[COURSES] UPDATE_COURSES_LIST',
  UPDATE_COURSES_LIST_SUCCESS = '[COURSES] UPDATE_COURSES_LIST_SUCCESS',
  CANCEL = '[COURSES] CANCEL'
}

@Injectable()
export class GetCoursesAction implements Action {
  readonly type = CoursesActionTypes.GET_COURSES;
  constructor(public payload: CourseParams) {}
}

export class GetCoursesSuccessAction implements Action {
  readonly type = CoursesActionTypes.GET_COURSES_SUCCESS;
  constructor(public payload: CoursesList) {}
}

export class GetCourseByIDAction implements Action {
  readonly type = CoursesActionTypes.GET_COURSE_BY_ID;
  constructor(public payload: CourseId) {}
}

export class GetCourseByIDSuccessAction implements Action {
  readonly type = CoursesActionTypes.GET_COURSE_BY_ID_SUCCESS;
  constructor(public payload: Course) {}
}

export class GetCourseByIDFailureAction implements Action {
  readonly type = CoursesActionTypes.GET_COURSE_BY_ID_FAILURE;
}

export class GetAuthorsAction implements Action {
  readonly type = CoursesActionTypes.GET_AUTHORS;
}

export class GetAuthorsSuccessAction implements Action {
  readonly type = CoursesActionTypes.GET_AUTHORS_SUCCESS;
  constructor(public payload: CourseAuthors) {}
}

export class UpdateCoursesListAction implements Action {
  readonly type = CoursesActionTypes.UPDATE_COURSES_LIST;
  constructor(public payload: CourseCountParams) {}
}

export class UpdateCoursesListSuccessAction implements Action {
  readonly type = CoursesActionTypes.UPDATE_COURSES_LIST_SUCCESS;
  constructor(public payload: CoursesList) {}
}

export class AddCourseAction implements Action {
  readonly type = CoursesActionTypes.ADD_COURSE;
  constructor(public payload: Course) {}
}

export class EditCourseAction implements Action {
  readonly type = CoursesActionTypes.EDIT_COURSE;
  constructor(public payload: Course) {}
}

export class SearchAction implements Action {
  readonly type = CoursesActionTypes.SEARCH;
  constructor(public payload: CourseVideo) {}
}

export class SearchSuccessAction implements Action {
  readonly type = CoursesActionTypes.SEARCH_SUCCESSS;
  constructor(public payload: CoursesList) {}
}

export class InitialStateAction implements Action {
  readonly type = CoursesActionTypes.INITIAL_STATE;
}

export class RemoveCourseAction implements Action {
  readonly type = CoursesActionTypes.REMOVE_COURSE;
  constructor(public payload: CourseId) {}
}

export class RemoveCourseSuccessAction implements Action {
  readonly type = CoursesActionTypes.REMOVE_COURSE_SUCCESS;
}

export class CancelAction implements Action {
  readonly type = CoursesActionTypes.CANCEL;
}

export type CoursesActions =
  | GetCoursesAction
  | GetCoursesSuccessAction
  | GetCourseByIDAction
  | GetCourseByIDSuccessAction
  | GetCourseByIDFailureAction
  | GetAuthorsAction
  | GetAuthorsSuccessAction
  | UpdateCoursesListAction
  | UpdateCoursesListSuccessAction
  | AddCourseAction
  | EditCourseAction
  | SearchAction
  | SearchSuccessAction
  | InitialStateAction
  | RemoveCourseAction
  | RemoveCourseSuccessAction
  | CancelAction;

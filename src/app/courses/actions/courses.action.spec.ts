import {
  AddCourseAction, CancelAction, CoursesActionTypes, EditCourseAction,
  GetAuthorsAction,
  GetAuthorsSuccessAction,
  GetCourseByIDAction,
  GetCourseByIDFailureAction,
  GetCourseByIDSuccessAction,
  GetCoursesAction,
  GetCoursesSuccessAction, InitialStateAction, RemoveCourseAction,
  RemoveCourseSuccessAction,
  SearchAction,
  SearchSuccessAction,
  UpdateCoursesListAction,
  UpdateCoursesListSuccessAction
} from './courses.action';
import {CourseItem} from '../models/course-item.model';

describe('GetCoursesAction', () => {
  it('should create an action', () => {
    const action = new GetCoursesAction({ start: '0', count: '10' });

    expect(action.type).toEqual(CoursesActionTypes.GET_COURSES);
  });
});

describe('GetCoursesSuccessAction', () => {
  it('should create an action', () => {
    const action = new GetCoursesSuccessAction({ coursesList: [] });

    expect(action.type).toEqual(CoursesActionTypes.GET_COURSES_SUCCESS);
  });
});

describe('GetCourseByIDAction', () => {
  it('should create an action', () => {
    const action = new GetCourseByIDAction({ id: 1 });

    expect(action.type).toEqual(CoursesActionTypes.GET_COURSE_BY_ID);
  });
});

describe('GetCourseByIDSuccessAction', () => {
  it('should create an action', () => {
    const action = new GetCourseByIDSuccessAction({ course: new CourseItem({
        name: '',
        id: 1,
        length: 1,
        date: '',
        isTopRated: false,
        description: '',
        authors: [],
      })
    });

    expect(action.type).toEqual(CoursesActionTypes.GET_COURSE_BY_ID_SUCCESS);
  });
});

describe('GetAuthorsAction', () => {
  it('should create an action', () => {
    const action = new GetAuthorsAction;

    expect(action.type).toEqual(CoursesActionTypes.GET_AUTHORS);
  });
});

describe('GetAuthorsSuccessAction', () => {
  it('should create an action', () => {
    const action = new GetAuthorsSuccessAction({ authors: [] });

    expect(action.type).toEqual(CoursesActionTypes.GET_AUTHORS_SUCCESS);
  });
});

describe('GetCourseByIDFailureAction', () => {
  it('should create an action', () => {
    const action = new GetCourseByIDFailureAction;

    expect(action.type).toEqual(CoursesActionTypes.GET_COURSE_BY_ID_FAILURE);
  });
});

describe('UpdateCoursesListAction', () => {
  it('should create an action', () => {
    const action = new UpdateCoursesListAction({ count: 1 });

    expect(action.type).toEqual(CoursesActionTypes.UPDATE_COURSES_LIST);
  });
});

describe('UpdateCoursesListSuccessAction', () => {
  it('should create an action', () => {
    const action = new UpdateCoursesListSuccessAction({ coursesList: [] });

    expect(action.type).toEqual(CoursesActionTypes.UPDATE_COURSES_LIST_SUCCESS);
  });
});

describe('AddCourseAction', () => {
  it('should create an action', () => {
    const action = new AddCourseAction({ course: new CourseItem({
        name: '',
        id: 1,
        length: 1,
        date: '',
        isTopRated: false,
        description: '',
        authors: [],
      })
    });

    expect(action.type).toEqual(CoursesActionTypes.ADD_COURSE);
  });
});

describe('EditCourseAction', () => {
  it('should create an action', () => {
    const action = new EditCourseAction({ course: new CourseItem({
        name: '',
        id: 1,
        length: 1,
        date: '',
        isTopRated: false,
        description: '',
        authors: [],
      }) });

    expect(action.type).toEqual(CoursesActionTypes.EDIT_COURSE);
  });
});

describe('SearchAction', () => {
  it('should create an action', () => {
    const action = new SearchAction({ video: '' });

    expect(action.type).toEqual(CoursesActionTypes.SEARCH);
  });
});

describe('SearchSuccessAction', () => {
  it('should create an action', () => {
    const action = new SearchSuccessAction({ coursesList: [] });

    expect(action.type).toEqual(CoursesActionTypes.SEARCH_SUCCESSS);
  });
});

describe('InitialStateAction', () => {
  it('should create an action', () => {
    const action = new InitialStateAction;

    expect(action.type).toEqual(CoursesActionTypes.INITIAL_STATE);
  });
});

describe('RemoveCourseAction', () => {
  it('should create an action', () => {
    const action = new RemoveCourseAction({ id: 1 });

    expect(action.type).toEqual(CoursesActionTypes.REMOVE_COURSE);
  });
});

describe('RemoveCourseSuccessAction', () => {
  it('should create an action', () => {
    const action = new RemoveCourseSuccessAction;

    expect(action.type).toEqual(CoursesActionTypes.REMOVE_COURSE_SUCCESS);
  });
});

describe('CancelAction', () => {
  it('should create an action', () => {
    const action = new CancelAction;

    expect(action.type).toEqual(CoursesActionTypes.CANCEL);
  });
});

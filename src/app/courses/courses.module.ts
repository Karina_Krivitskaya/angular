import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import { ToolboxComponent } from './components/toolbox/toolbox.component';
import { CoursesListComponent } from './components/courses-list/courses-list.component';
import { CoursesListItemComponent } from './components/courses-list-item/courses-list-item.component';
import { SelectCourseDirective } from './directives/select-course/select-course.directive';
import { FormatTimePipe } from './pipes/format-time.pipe';
import { FilterByPipe } from './pipes/filter-by.pipe';
import { CoursesService } from './services/courses.service';
import { DateComponent } from './components/date/date.component';
import { DurationTimeComponent } from './components/duration-time/duration-time.component';
import {RouterModule, Routes} from '@angular/router';
import { CoursesPageComponent } from './components/courses-page/courses-page.component';
import {CoreModule} from '../core/core.module';
import {CanActivateGuardService} from '../core/services/guards/can-activate-guard.service';
import {CourseFormComponent} from './components/course-form/course-form.component';
import {HttpClientModule} from '@angular/common/http';
import { AuthorsComponent } from './components/authors/authors/authors.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {TagInputModule} from 'ngx-chips';
import { UpDownComponent } from './components/up-down/up-down.component';

const appRoutes: Routes = [
  { path: 'courses', component: CoursesPageComponent, children: [
      { path: '', component: CoursesListComponent},
      { path: 'new', component: CourseFormComponent},
      { path: ':id', component: CourseFormComponent}
    ], canActivate: [CanActivateGuardService]}
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    TagInputModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    CoreModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes)
  ],
  declarations: [
    ToolboxComponent,
    CoursesListComponent,
    CoursesListItemComponent,
    SelectCourseDirective,
    FormatTimePipe,
    FilterByPipe,
    CourseFormComponent,
    DateComponent,
    DurationTimeComponent,
    CoursesPageComponent,
    AuthorsComponent,
    UpDownComponent],
  exports: [CoursesListComponent, SelectCourseDirective],
  providers: [CoursesService]
})
export class CoursesModule { }

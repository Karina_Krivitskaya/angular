import * as fromCourses from './courses.reducer';
import {
  GetAuthorsSuccessAction,
  GetCourseByIDSuccessAction, GetCoursesAction, GetCoursesSuccessAction, InitialStateAction, SearchAction, SearchSuccessAction,
  UpdateCoursesListSuccessAction
} from '../actions/courses.action';
import {CourseItem} from '../models/course-item.model';

describe('coursesReducer', () => {
  describe('undefined action', () => {
    it('should return the default state', () => {
      const { initialState } = fromCourses;
      const state = fromCourses.coursesReducer(undefined, new GetCoursesAction({ start: '', count: '' }));

      expect(state).toBe(initialState);
    });
  });

  describe('GetCoursesSuccess action', () => {
    it('should return correct state', () => {
      const responseData = [
        {
          id: 1,
          name: 'duis mollit reprehenderit ad',
          description: 'abc',
          isTopRated: false,
          date: '2017-09-28T04:39:24+00:00',
          authors: [],
          length: 157
        },
        {
          id: 2,
          name: 'duis mollit reprehenderit ad',
          description: 'abc',
          isTopRated: false,
          date: '2017-09-28T04:39:24+00:00',
          authors: [],
          length: 157
        },
        {
          id: 3,
          name: 'duis mollit reprehenderit ad',
          description: 'abc',
          isTopRated: false,
          date: '2017-09-28T04:39:24+00:00',
          authors: [],
          length: 157
        },
        {
          id: 4,
          name: 'duis mollit reprehenderit ad',
          description: 'abc',
          isTopRated: false,
          date: '2017-09-28T04:39:24+00:00',
          authors: [],
          length: 157
        },
        {
          id: 5,
          name: 'duis mollit reprehenderit ad',
          description: 'abc',
          isTopRated: false,
          date: '2017-09-28T04:39:24+00:00',
          authors: [],
          length: 157
        },
        {
          id: 6,
          name: 'duis mollit reprehenderit ad',
          description: 'abc',
          isTopRated: false,
          date: '2017-09-28T04:39:24+00:00',
          authors: [],
          length: 157
        }
      ];
      const { initialState } = fromCourses;
      const state = fromCourses.coursesReducer(initialState, new GetCoursesSuccessAction({
        coursesList: responseData
      }));

      expect(state.coursesList).toEqual(responseData.slice(0, 5));
      expect(state.loadCourses).toBeTruthy();
    });
  });

  describe('GetCourseByIDSuccess action', () => {
    it('should return correct state', () => {
      const responseData = {
        id: 6,
        name: 'duis mollit reprehenderit ad',
        description: 'abc',
        isTopRated: false,
        date: '2017-09-28T04:39:24+00:00',
        authors: [],
        length: 157
      };
      const { initialState } = fromCourses;
      const state = fromCourses.coursesReducer(initialState, new GetCourseByIDSuccessAction({
        course: responseData
      }));

      expect(state.editedCourse).toEqual(responseData);
    });
  });

  describe('GetAuthorsSuccess action', () => {
    it('should return correct state', () => {
      const responseData = [{
        id: '6',
        name: 'name'
      }];
      const { initialState } = fromCourses;
      const state = fromCourses.coursesReducer(initialState, new GetAuthorsSuccessAction({
        authors: responseData
      }));

      expect(state.authors).toEqual(responseData);
    });
  });

  describe('UpdateCoursesListSuccess action', () => {
    it('should return correct state', () => {
      const responseData = [
        {
          id: 1,
          name: 'duis mollit reprehenderit ad',
          description: 'abc',
          isTopRated: false,
          date: '2017-09-28T04:39:24+00:00',
          authors: [],
          length: 157
        },
        {
          id: 2,
          name: 'duis mollit reprehenderit ad',
          description: 'abc',
          isTopRated: false,
          date: '2017-09-28T04:39:24+00:00',
          authors: [],
          length: 157
        },
        {
          id: 3,
          name: 'duis mollit reprehenderit ad',
          description: 'abc',
          isTopRated: false,
          date: '2017-09-28T04:39:24+00:00',
          authors: [],
          length: 157
        },
        {
          id: 4,
          name: 'duis mollit reprehenderit ad',
          description: 'abc',
          isTopRated: false,
          date: '2017-09-28T04:39:24+00:00',
          authors: [],
          length: 157
        },
        {
          id: 5,
          name: 'duis mollit reprehenderit ad',
          description: 'abc',
          isTopRated: false,
          date: '2017-09-28T04:39:24+00:00',
          authors: [],
          length: 157
        },
        {
          id: 6,
          name: 'duis mollit reprehenderit ad',
          description: 'abc',
          isTopRated: false,
          date: '2017-09-28T04:39:24+00:00',
          authors: [],
          length: 157
        }
      ];
      const { initialState } = fromCourses;
      const state = fromCourses.coursesReducer(initialState, new UpdateCoursesListSuccessAction({
        coursesList: responseData
      }));

      expect(state.coursesList).toEqual(responseData);
    });
  });

  describe('Search action', () => {
    it('should return correct state', () => {
      const { initialState } = fromCourses;
      const state = fromCourses.coursesReducer(initialState, new SearchAction({
        video: 'mollit'
      }));

      expect(state.searchName).toEqual('mollit');
    });
  });

  describe('SearchSuccess action', () => {
    it('should return correct state', () => {
      const responseData = [
        new CourseItem({
          id: 1,
          name: 'duis mollit reprehenderit ad',
          description: 'abc',
          isTopRated: false,
          date: '2017-09-28T04:39:24+00:00',
          authors: null,
          length: 157
        }),
        new CourseItem({
          id: 2,
          name: 'duis mollit reprehenderit ad',
          description: 'abc',
          isTopRated: false,
          date: '2017-09-28T04:39:24+00:00',
          authors: null,
          length: 157
        })
      ];
      const { initialState } = fromCourses;
      const state = fromCourses.coursesReducer(initialState, new SearchSuccessAction({
        coursesList: responseData
      }));

      expect(state.coursesList).toEqual(responseData);
      expect(state.loadCourses).toBeFalsy();
      expect(state.errorMessage).toEqual(null);
    });

    it('should return correct error message if do not found courses', () => {
      const responseData = [];
      const { initialState } = fromCourses;
      const state = fromCourses.coursesReducer(initialState, new SearchSuccessAction({
        coursesList: responseData
      }));

      expect(state.coursesList).toEqual(responseData);
      expect(state.loadCourses).toBeFalsy();
      expect(state.errorMessage).toEqual('No such courses');
    });
  });

  describe('InitialState action', () => {
    it('should return the default state', () => {
      const { initialState } = fromCourses;
      const state = fromCourses.coursesReducer(undefined, new InitialStateAction);

      expect(state).toBe(initialState);
    });
  });
});

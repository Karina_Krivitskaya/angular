import {
  CoursesActionTypes, CoursesActions
} from '../actions/courses.action';
import {COUNT_ITEM_LOAD} from '../constants/courses.constants';

import {CourseItem} from '../models/course-item.model';
import {AuthorModel} from '../models/author.model';

export interface State {
  coursesList: CourseItem[];
  editedCourse: CourseItem;
  authors: AuthorModel[];
  loadCourses: boolean;
  searchName: string;
  errorMessage: string;
}

export const initialState: State = {
  coursesList: [],
  editedCourse: null,
  authors: [],
  loadCourses: true,
  searchName: null,
  errorMessage: null
};

export function coursesReducer(state = initialState, action: CoursesActions) {
  switch (action.type) {
    case CoursesActionTypes.GET_COURSES_SUCCESS: {
      return {
        ...state,
        coursesList: state.coursesList.concat(action.payload.coursesList.slice(0, COUNT_ITEM_LOAD)),
        loadCourses: action.payload.coursesList.length > COUNT_ITEM_LOAD
      };
    }
    case CoursesActionTypes.GET_COURSE_BY_ID_SUCCESS: {
      return {
        ...state,
        editedCourse: action.payload.course
      };
    }
    case CoursesActionTypes.GET_AUTHORS_SUCCESS: {
      return {
        ...state,
        authors: action.payload.authors
      };
    }
    case CoursesActionTypes.UPDATE_COURSES_LIST_SUCCESS: {
      return {
        ...state,
        coursesList: action.payload.coursesList
      };
    }
    case CoursesActionTypes.SEARCH: {
      return {
        ...state,
        searchName: action.payload.video
      };
    }
    case CoursesActionTypes.SEARCH_SUCCESSS: {
      return {
        ...state,
        coursesList: action.payload.coursesList,
        loadCourses: false,
        errorMessage: !action.payload.coursesList.length ? 'No such courses' : null
      };
    }
    case CoursesActionTypes.INITIAL_STATE: {
      return initialState;
    }
    default: {
      return state;
    }
  }
}

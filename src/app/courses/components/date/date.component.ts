import {Component, forwardRef} from '@angular/core';
import {ControlValueAccessor, FormControl, NG_VALIDATORS, NG_VALUE_ACCESSOR, Validator} from '@angular/forms';
import {DATE_FORMAT} from '../../constants/courses.constants';

@Component({
  selector: 'app-date',
  templateUrl: './date.component.html',
  styleUrls: ['./date.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DateComponent),
      multi: true,
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => DateComponent),
      multi: true,
    }
  ]
})
export class DateComponent implements ControlValueAccessor, Validator {
  public date: string;
  public valid: boolean;

  constructor() { }

  changeDate(date: string) {
    this.onChange(date);
    this.onTouched();
  }

  writeValue(value) {
    this.date = value;
  }

  onChange: any = (date: string) => { };

  onTouched: any = () => { };

  registerOnChange(fn) {
    this.onChange = fn;
  }

  registerOnTouched(fn) {
    this.onTouched = fn;
  }

  public validate(form: FormControl) {
    if (!form.value && form.dirty) {
      this.valid = false;
      return {
        required: {
          valid: false
        }
      };
    } else if (!form.value) {
      this.valid = true;
      return null;
    }

    if (form.value && DATE_FORMAT.test(form.value)) {
      const date = form.value.split('.');

      if (date[0] <= 12 && date[0] >= 1 && date[1] <= 31 && date[1] >= 1) {
        this.valid = true;
        return null;
      }
    }

    this.valid = false;
    return {
      formatDate: {
        valid: false
      }
    };
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DateComponent } from './date.component';
import {FormControl, FormsModule} from '@angular/forms';

describe('DateComponent', () => {
  let component: DateComponent;
  let fixture: ComponentFixture<DateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DateComponent ],
      imports: [ FormsModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('onChange method should exist', () => {
    component.onChange('');
    expect(component.onChange).toBeTruthy();
  });

  it('changeDuration method should call onChange method', () => {
    spyOn(component, 'onChange');
    component.changeDate('11.11.2019');
    expect(component.onChange).toHaveBeenCalledWith('11.11.2019');
  });

  it('writeValue method should write value', () => {
    component.writeValue('11.11.2019');
    expect(component.date).toEqual('11.11.2019');
  });

  it('validate method should return null if form is valid', () => {
    const form = new FormControl();
    form.setValue('11.11.2019');
    expect(component.validate(form)).toEqual(null);
  });

  it('validate method should return correct error if form value is not date', () => {
    const form = new FormControl();
    form.setValue('1134.2019');
    expect(component.validate(form)).toEqual({
      formatDate: {
        valid: false
      }
    });
  });

  it('validate method should return correct error if form value is not correct date', () => {
    const form = new FormControl();
    form.setValue('11.34.2019');
    expect(component.validate(form)).toEqual({
      formatDate: {
        valid: false
      }
    });
  });

  it('validate method should return correct error if form value is not exist and touched', () => {
    const form = new FormControl();
    form.setValue('');
    form.markAsDirty();
    expect(component.validate(form)).toEqual({
      required: {
        valid: false
      }
    });
  });
});

import {Component, forwardRef, Input} from '@angular/core';
import {ControlValueAccessor, FormControl, NG_VALIDATORS, NG_VALUE_ACCESSOR, Validator} from '@angular/forms';
import {AuthorModel} from '../../../models/author.model';

@Component({
  selector: 'app-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => AuthorsComponent),
      multi: true,
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => AuthorsComponent),
      multi: true,
    }
  ]
})

export class AuthorsComponent implements ControlValueAccessor, Validator {
  public valid: boolean;
  public courseAuthors: AuthorModel[];
  @Input() authors: AuthorModel[];

  changeAuthors(items) {
    this.onChange(items);
    this.onTouched();
  }

  writeValue(value) {
    if (value) {
      this.courseAuthors = value.map(author => {
        if (!author.name) {
          return {
            name: author.firstName + ' ' + author.lastName,
            id: author.id
          };
        }
        return author;
      });
    }
  }

  onChange: any = (courseAuthors: string) => { };

  onTouched: any = () => { };

  registerOnChange(fn) {
    this.onChange = fn;
  }

  registerOnTouched(fn) {
    this.onTouched = fn;
  }

  public validate(form: FormControl) {
    if ((!form.value || !form.value.length) && form.dirty) {
      this.valid = false;
      return {
        required: {
          valid: false
        }
      };
    }

    this.valid = true;
    return null;
  }
}

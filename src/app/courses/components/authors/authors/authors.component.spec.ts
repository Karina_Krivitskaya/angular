import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthorsComponent } from './authors.component';
import {TagInputModule} from 'ngx-chips';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormControl, FormsModule, ReactiveFormsModule} from '@angular/forms';

describe('AuthorsComponent', () => {
  let component: AuthorsComponent;
  let fixture: ComponentFixture<AuthorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        TagInputModule,
        BrowserModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        FormsModule
      ],
      declarations: [ AuthorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('onChange method should exist', () => {
    component.onChange([]);
    expect(component.onChange).toBeTruthy();
  });

  it('changeDuration method should call onChange method', () => {
    spyOn(component, 'onChange');
    component.changeAuthors([]);
    expect(component.onChange).toHaveBeenCalledWith([]);
  });

  it('writeValue method should write value if exist one', () => {
    const authors = [{
      name: 'first last',
      id: '1'
    }, {
      firstName: 'first',
      lastName: 'last',
      id: '2'
    }];
    component.writeValue(authors);
    expect(component.courseAuthors).toEqual([
      {
        name: 'first last',
        id: '1'
      },
      {
        name: 'first last',
        id: '2'
      }
    ]);
  });

  it('writeValue method should not write value if value empty', () => {
    component.writeValue(null);
    expect(component.courseAuthors).toEqual(undefined);
  });

  it('validate method should return null if form is valid', () => {
    const form = new FormControl();
    form.setValue('100');
    expect(component.validate(form)).toEqual(null);
  });

  it('validate method should return correct error if form value is empty or not exist', () => {
    const form = new FormControl();
    form.setValue([]);
    form.markAsDirty();
    expect(component.validate(form)).toEqual({
      required: {
        valid: false
      }
    });

    form.setValue(null);
    expect(component.validate(form)).toEqual({
      required: {
        valid: false
      }
    });
  });
});

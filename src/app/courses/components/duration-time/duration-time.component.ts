import {Component, forwardRef} from '@angular/core';
import {ControlValueAccessor, FormControl, NG_VALIDATORS, NG_VALUE_ACCESSOR, Validator} from '@angular/forms';
import {NUMBER} from '../../constants/courses.constants';

@Component({
  selector: 'app-duration-time',
  templateUrl: './duration-time.component.html',
  styleUrls: ['./duration-time.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DurationTimeComponent),
      multi: true,
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => DurationTimeComponent),
      multi: true,
    }
  ]
})

export class DurationTimeComponent implements ControlValueAccessor, Validator {
  public duration: string;
  public valid: boolean;

  constructor() { }

  changeDuration(duration: string) {
    this.onChange(duration);
    this.onTouched();
  }

  writeValue(value) {
    this.duration = value;
  }

  onChange: any = (duration: string) => { };

  onTouched: any = () => { };

  registerOnChange(fn) {
    this.onChange = fn;
  }

  registerOnTouched(fn) {
    this.onTouched = fn;
  }

  public validate(form: FormControl) {
    if (!form.value && form.dirty) {
      this.valid = false;
      return {
        required: {
          valid: false
        }
      };
    }

    if (form.value && !NUMBER.test(form.value)) {
      this.valid = false;
      return {
        formatTime: {
          valid: false
        }
      };
    }

    this.valid = true;
    return null;
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DurationTimeComponent } from './duration-time.component';
import {FormControl, FormsModule} from '@angular/forms';
import {FormatTimePipe} from '../../pipes/format-time.pipe';

describe('DurationTimeComponent', () => {
  let component: DurationTimeComponent;
  let fixture: ComponentFixture<DurationTimeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DurationTimeComponent, FormatTimePipe ],
      imports: [ FormsModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DurationTimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('onChange method should exist', () => {
    component.onChange('');
    expect(component.onChange).toBeTruthy();
  });

  it('changeDuration method should call onChange method', () => {
    spyOn(component, 'onChange');
    component.changeDuration('100');
    expect(component.onChange).toHaveBeenCalledWith('100');
  });

  it('writeValue method should write value', () => {
    component.writeValue('100');
    expect(component.duration).toEqual('100');
  });

  it('validate method should return null if form is valid', () => {
    const form = new FormControl();
    form.setValue('100');
    expect(component.validate(form)).toEqual(null);
  });

  it('validate method should return correct error if form value is not number', () => {
    const form = new FormControl();
    form.setValue('abc');
    expect(component.validate(form)).toEqual({
      formatTime: {
        valid: false
      }
    });
  });

  it('validate method should return correct error if form value is not exist and touched', () => {
    const form = new FormControl();
    form.setValue('');
    form.markAsDirty();
    expect(component.validate(form)).toEqual({
      required: {
        valid: false
      }
    });
  });
});

import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable, Subscription} from 'rxjs';
import {LoadingService} from '../../../core/services/loading/loading.service';
import {Store} from '@ngrx/store';
import {AppState, selectCoursesState} from '../../../store/app.state';
import {
  AddCourseAction, CancelAction, EditCourseAction, GetAuthorsAction, GetCourseByIDAction
} from '../../actions/courses.action';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DatePipe} from '@angular/common';
import {AuthorModel} from '../../models/author.model';

@Component({
  selector: 'app-course-form',
  templateUrl: './course-form.component.html',
  styleUrls: ['./course-form.component.css'],
  providers: [DatePipe]
})
export class CourseFormComponent implements OnInit, OnDestroy {
  private routeSubscription: Subscription;
  public courseId: number;
  public courseAuthors: AuthorModel[];
  public courseForm: FormGroup;
  private getStateSubscription$: Subscription;
  private getState$: Observable<any>;
  public course = {
    name: '',
    id: null,
    length: null,
    date: null,
    isTopRated: false,
    description: '',
    authors: null
  };

  get name() { return this.courseForm.get('name'); }
  get description() { return this.courseForm.get('description'); }
  get date() { return this.courseForm.get('date'); }
  get length() { return this.courseForm.get('length'); }
  get authors() { return this.courseForm.get('authors'); }

  constructor(private route: ActivatedRoute,
              private router: Router,
              private fb: FormBuilder,
              private datePipe: DatePipe,
              private store: Store<AppState>,
              private loadingService: LoadingService) {
    this.getState$ = this.store.select(selectCoursesState);
  }

  ngOnInit() {
    this.courseForm = this.fb.group({
      name: ['', [Validators.required, Validators.maxLength(50)]],
      description: ['', [Validators.required, Validators.maxLength(500)]],
      date: ['', Validators.required],
      length: ['', Validators.required],
      authors: [[], Validators.required]
    });

    this.getStateSubscription$ = this.getState$.subscribe(state => {
      this.course = state.editedCourse || this.course;
      this.courseAuthors = state.authors;

      this.courseForm.patchValue({
        name: this.course.name,
        description: this.course.description,
        date: this.datePipe.transform(this.course.date, 'MM.dd.yyyy'),
        length: this.course.length,
        authors: this.course.authors
      });
    });

    this.getCourse();
  }

  public getCourse() {
    this.routeSubscription = this.route.paramMap.subscribe(params => this.courseId = +params.get('id'));
    this.store.dispatch(new GetAuthorsAction);

    if (this.courseId) {
      this.loadingService.activate();
      this.store.dispatch(new GetCourseByIDAction({ id: this.courseId }));
    }
  }

  public save() {
    if (this.courseForm.invalid) {
      this.makeFormDirty();
      return;
    }

    this.loadingService.activate();

    if (this.courseId) {
      this.store.dispatch(new EditCourseAction({ course: this.getEditedCourse() }));
    } else {
      this.store.dispatch(new AddCourseAction({ course: this.courseForm.value }));
    }
  }

  public makeFormDirty() {
    Object.keys(this.courseForm.controls).forEach(key => {
      const form = this.courseForm.get(key);

      form.markAsDirty();
      form.validator(form);
    });
  }

  public getEditedCourse() {
    return {
      ...this.courseForm.value,
      id: this.course.id,
      authors: this.getFormattedAuthors()
    };
  }

  public getFormattedAuthors() {
    return this.authors.value.map(author => {
      if (author.firstName && author.lastName) {
        return author;
      }
      const name = author.name.split(' ');
      return {
        id: author.id,
        firstName: name[0],
        lastName: name[1]
      };
    });
  }

  public cancel() {
    this.store.dispatch(new CancelAction);
  }

  ngOnDestroy() {
    this.getStateSubscription$.unsubscribe();
    this.routeSubscription.unsubscribe();
  }

}

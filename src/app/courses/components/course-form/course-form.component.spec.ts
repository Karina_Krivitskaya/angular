import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {FormBuilder, FormsModule, ReactiveFormsModule} from '@angular/forms';
import {DurationTimeComponent} from '../duration-time/duration-time.component';
import {DateComponent} from '../date/date.component';
import {CUSTOM_ELEMENTS_SCHEMA, Pipe, PipeTransform} from '@angular/core';
import {FormatTimePipe} from '../../pipes/format-time.pipe';
import {ActivatedRoute, Router} from '@angular/router';
import {RouterTestingModule} from '@angular/router/testing';
import {CoursesPageComponent} from '../courses-page/courses-page.component';
import { convertToParamMap, ParamMap, Params } from '@angular/router';
import {of, ReplaySubject} from 'rxjs';
import {ErrorPageComponent} from '../../../core/components/error-page/error-page.component';
import {CourseFormComponent} from './course-form.component';
import {LoadingService} from '../../../core/services/loading/loading.service';
import {Store} from '@ngrx/store';
import {
  AddCourseAction, CancelAction, EditCourseAction, GetAuthorsAction, GetCourseByIDAction
} from '../../actions/courses.action';
import {DatePipe} from '@angular/common';
import {BrowserModule} from '@angular/platform-browser';
import {AuthorsComponent} from '../authors/authors/authors.component';
import {TagInputModule} from 'ngx-chips';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';


class TestStore {
  constructor(private state: any) {
    this.state = state;
  }
  dispatch = () => undefined;
  select = () => of(this.state);
}

describe('CourseFormComponent', () => {
  let component: CourseFormComponent;
  let fixture: ComponentFixture<CourseFormComponent>;
  let LoadingServiceMock;
  let router;
  let store;
  let loadingService;
  let fb;
  let datePipeMock;
  let datePipe;
  const appRoutes = [
    { path: 'courses', component: CoursesPageComponent }
  ];

  @Pipe({
    name: 'date',
    pure: false
  })
  class MockedDatePipe implements PipeTransform {
    name = 'date';

    transform(query: string, ...args: any[]): any {
      return query;
    }
  }
  const declarations = [
    CourseFormComponent,
    AuthorsComponent,
    DurationTimeComponent,
    DateComponent,
    FormatTimePipe,
    CoursesPageComponent,
    ErrorPageComponent];

  class ActivatedRouteStub {
    private subject = new ReplaySubject<ParamMap>();

    constructor(initialParams?: Params) {
    this.setParamMap(initialParams);
  }

  readonly paramMap = this.subject.asObservable();

  setParamMap(params?: Params) {
    this.subject.next(convertToParamMap(params));
  }
}

  let activatedRoute: ActivatedRouteStub;

  const createComponent = (activatedRouteStub: ActivatedRouteStub, selectData) => {
    LoadingServiceMock = {
      activate: () => {}
    };
    datePipeMock = {
      transform: () => {}
    };
    TestBed.configureTestingModule({
      declarations: declarations,
      imports: [
        FormsModule,
        TagInputModule,
        BrowserAnimationsModule,
        BrowserModule,
        RouterTestingModule.withRoutes(appRoutes),
        ReactiveFormsModule
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        FormBuilder,
        {provide: ActivatedRoute, useValue: activatedRouteStub},
        {provide: LoadingService, useValue: LoadingServiceMock},
        {provide: Store, useValue: new TestStore(selectData)},
        {provide: DatePipe, useValue: datePipeMock}]
    })
    .compileComponents();
    fixture = TestBed.createComponent(CourseFormComponent);
    component = fixture.componentInstance;
    store = fixture.debugElement.injector.get(Store);
    datePipe = TestBed.get(DatePipe);

    loadingService = TestBed.get(LoadingService);
    spyOn(loadingService, 'activate');
    spyOn(store, 'dispatch');
    spyOn(datePipe, 'transform');

    router = fixture.debugElement.injector.get(Router);
    fb = TestBed.get(FormBuilder);

    component.courseAuthors = [];
    component.courseForm = fb.group({
      name: [''],
      description: [''],
      date: [''],
      length: [''],
      authors: [[]]
    });

    spyOn(router, 'navigate');
    fixture.detectChanges();
  };

  function setForm() {
    component.courseForm.controls['name'].setValue('name');
    component.courseForm.controls['description'].setValue('description');
    component.courseForm.controls['date'].setValue('11.11.2019');
    component.courseForm.controls['length'].setValue(100);
    component.courseForm.controls['authors'].setValue([{
      id: 1,
      firstName: 'first',
      lastName: 'last'
    }]);
  }

  beforeEach(async(() => {
    activatedRoute = new ActivatedRouteStub();
  }));

  it('should create', () => {
    createComponent(activatedRoute, {
      coursesList: [],
      editedCourse: {},
      loadCourses: true,
      searchName: null,
      errorMessage: null,
      authors: []
    });
    expect(component).toBeTruthy();
  });

  it('ngOnInit method should not change course if editedCourse state is empty', () => {
    createComponent(activatedRoute, {
      coursesList: [],
      editedCourse: null,
      loadCourses: true,
      searchName: null,
      errorMessage: null,
      authors: []
    });
    expect(component.course).toEqual({
      name: '',
      id: null,
      length: null,
      date: null,
      isTopRated: false,
      description: '',
      authors: null
    });
  });

  it('getCourse method should correctly init course if courseId undefined', () => {
    const action = new GetAuthorsAction;
    createComponent(activatedRoute, {
      coursesList: [],
      editedCourse: {},
      loadCourses: true,
      searchName: null,
      errorMessage: null,
      authors: []
    });

    store.dispatch.calls.reset();
    component.getCourse();
    expect(store.dispatch).toHaveBeenCalledWith(action);
    expect(store.dispatch).toHaveBeenCalledTimes(1);
  });

  it('getCourse method should correctly init course if courseId defined', () => {
    const action = new GetCourseByIDAction({ id: 1 });
    activatedRoute.setParamMap({ id: 1 });
    const data = {
      id: 5688,
      name: 'ex laborum est cupidatat',
      description: 'Quis tempor eiusmod esse id minim anim.',
        isTopRated: false,
        date: '2017-11-18T09:06:11+00:00',
        authors: [],
        length: 171
    };
    createComponent(activatedRoute, {
      coursesList: [],
      editedCourse: data,
      loadCourses: true,
      searchName: null,
      errorMessage: null
    });

    component.getCourse();
    expect(loadingService.activate).toHaveBeenCalled();
    expect(store.dispatch).toHaveBeenCalledWith(action);
  });

  it('save method should dispatch state if it is a new one', () => {
    spyOn(store, 'dispatch');
    component.courseForm.setErrors(null);
    const course = {
      name: 'name',
      description: 'description',
      date: '11.11.2019',
      authors: [{
        id: 1,
        firstName: 'first',
        lastName: 'last'
      }],
      length: 100
    };
    const action = new AddCourseAction({ course: course });
    createComponent(activatedRoute, {
      coursesList: [],
      editedCourse: {},
      loadCourses: true,
      searchName: null,
      errorMessage: null,
      authors: []
    });

    setForm();
    store.dispatch.calls.reset();
    component.save();
    expect(store.dispatch).toHaveBeenCalledWith(action);
  });

  it('save method should dispatch state if it is edit form', () => {
    const action = new EditCourseAction({
      course: {
        id: 1,
        name: 'name',
        description: 'description',
        date: '11.11.2019',
        length: 100,
        authors: [{
          id: 1,
          firstName: 'first',
          lastName: 'last'
        }]
      }
    });
    activatedRoute.setParamMap({ id: 1 });
    createComponent(activatedRoute, {
      coursesList: [],
      editedCourse: {
        id: 1,
        name: 'Name',
        description: 'description',
        isTopRated: false,
        date: '11.11.2019',
        authors: [],
        length: 122
      },
      loadCourses: true,
      searchName: null,
      errorMessage: null
    });

    store.dispatch.calls.reset();
    setForm();
    component.save();
    expect(store.dispatch).toHaveBeenCalledWith(action);
  });

  it('makeFormDirty method should make all field as dirty if form invalid', () => {
    createComponent(activatedRoute, {
      coursesList: [],
      editedCourse: {},
      loadCourses: true,
      searchName: null,
      errorMessage: null
    });

    component.courseForm.controls['name'].setValue('');
    component.courseForm.controls['description'].setValue('');
    component.courseForm.controls['date'].setValue('');
    component.courseForm.controls['length'].setValue('');
    component.courseForm.controls['authors'].setValue([]);

    component.makeFormDirty();

    Object.keys(component.courseForm.controls).forEach(key => {
      const form = component.courseForm.get(key);

      expect(form.dirty).toBeTruthy();
    });
  });

  it('save method should call makeFormDirty method', () => {
    createComponent(activatedRoute, {
      coursesList: [],
      editedCourse: {},
      loadCourses: true,
      searchName: null,
      errorMessage: null
    });
    spyOn(component, 'makeFormDirty');

    component.save();
    expect(component.makeFormDirty).toHaveBeenCalled();
  });

  it('getFormattedAuthors method should prepare author data for dispatch', () => {
    createComponent(activatedRoute, {
      coursesList: [],
      editedCourse: {},
      loadCourses: true,
      searchName: null,
      errorMessage: null,
      author: []
    });

    component.authors.setValue([
      {
        name: 'first last',
        id: 1
      }
    ]);
    expect(component.getFormattedAuthors()).toEqual([{
      firstName: 'first',
      lastName: 'last',
      id: 1
    }]);
  });

  it('cancel method should dispatch state', () => {
    const action = new CancelAction;
    createComponent(activatedRoute, {
      coursesList: [],
      editedCourse: {},
      loadCourses: true,
      searchName: null,
      errorMessage: null
    });
    component.cancel();
    expect(store.dispatch).toHaveBeenCalledWith(action);
  });
});

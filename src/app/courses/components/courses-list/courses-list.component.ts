import {Component, OnDestroy, OnInit} from '@angular/core';
import {CourseItem} from '../../models/course-item.model';
import {Observable, Subscription} from 'rxjs';
import {LoadingService} from '../../../core/services/loading/loading.service';
import {Store} from '@ngrx/store';
import {AppState, selectCoursesState} from '../../../store/app.state';
import {
  GetCoursesAction, InitialStateAction, RemoveCourseAction, SearchAction, UpdateCoursesListAction
} from '../../actions/courses.action';
import {COUNT_ITEM_LOAD, COUNT_ITEM_REQUEST} from '../../constants/courses.constants';

@Component({
  selector: 'app-courses-list',
  templateUrl: './courses-list.component.html',
  styleUrls: ['./courses-list.component.css']
})
export class CoursesListComponent implements OnInit, OnDestroy {
  public coursesList: CourseItem[];
  public start: number;
  public loadCourse: boolean;
  public errorMessage: string;
  private getStateSubscription$: Subscription;
  private getState$: Observable<any>;

  constructor(private loadingService: LoadingService,
              private store: Store<AppState>) {
    this.getState$ = this.store.select(selectCoursesState);
  }

  ngOnInit() {
    this.start = 0;

    this.getStateSubscription$ = this.getState$.subscribe(
    (state) => {
        this.coursesList = state.coursesList;
        this.loadCourse = state.loadCourses;
        this.errorMessage = state.errorMessage;
      });

    this.getCourses(this.start, COUNT_ITEM_REQUEST);
  }

  public getCourses(start: number, count: number) {
    this.loadingService.activate();

    this.store.dispatch(new GetCoursesAction(
    { start: start.toString(), count: count.toString() }
    ));
  }

  public onDeleteCourse(id: number) {
    this.store.dispatch(new RemoveCourseAction({ id: id }));
  }

  public loadMore() {
    this.start += COUNT_ITEM_LOAD;
    this.getCourses(this.start, COUNT_ITEM_REQUEST);
  }

  public viewAllCourses() {
    this.start = 0;
    this.store.dispatch(new InitialStateAction);
    this.getCourses(this.start, COUNT_ITEM_REQUEST);
  }

  public filterByVideoTitle(video: string) {
    if (!video) {
      this.viewAllCourses();
      return;
    }

    this.loadingService.activate();
    this.store.dispatch(new SearchAction({ video: video }));
  }

  public updatePage() {
    this.loadingService.activate();
    this.start = 0;
    this.store.dispatch(new UpdateCoursesListAction( {count: COUNT_ITEM_LOAD} ));
  }

  ngOnDestroy() {
    this.getStateSubscription$.unsubscribe();
    this.store.dispatch(new InitialStateAction);
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CoursesListComponent } from './courses-list.component';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {CoursesListItemComponent} from '../courses-list-item/courses-list-item.component';
import {OrderByPipe} from '../../pipes/order-by.pipe';
import {SelectCourseDirective} from '../../directives/select-course/select-course.directive';
import {FormatTimePipe} from '../../pipes/format-time.pipe';
import {RouterTestingModule} from '@angular/router/testing';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CourseFormComponent} from '../course-form/course-form.component';
import {of} from 'rxjs';
import {LoadingService} from '../../../core/services/loading/loading.service';
import {Store} from '@ngrx/store';
import {GetCoursesAction, InitialStateAction, RemoveCourseAction, SearchAction} from '../../actions/courses.action';

class TestStore {
  constructor(private state: any) {
    this.state = state;
  }
  dispatch = () => undefined;
  select = () => of(this.state);
}

describe('CoursesListComponent', () => {
  let component: CoursesListComponent;
  let fixture: ComponentFixture<CoursesListComponent>;
  let orderBySpy: jasmine.Spy;
  let store;
  let loadingService;

  const selectData = {
    coursesList: [],
    editedCourse: null,
    loadCourses: true,
    searchName: null,
    errorMessage: null
  };

  const LoadingServiceMock = {
    activate: () => {}
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        CoursesListComponent,
        CoursesListItemComponent,
        OrderByPipe,
        FormatTimePipe,
        SelectCourseDirective,
        CourseFormComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [ FormsModule, ReactiveFormsModule, RouterTestingModule.withRoutes([
        { path: 'courses/:id', component: CourseFormComponent }
      ]) ],
      providers: [{provide: LoadingService, useValue: LoadingServiceMock},
        {provide: Store, useValue: new TestStore(selectData)}]
    })
    .compileComponents();
    fixture = TestBed.createComponent(CoursesListComponent);
    component = fixture.componentInstance;

    store = fixture.debugElement.injector.get(Store);
    loadingService = TestBed.get(LoadingService);

    spyOn(store, 'dispatch');
    spyOn(loadingService, 'activate');
    orderBySpy = spyOn(OrderByPipe.prototype, 'transform');
    spyOn(window, 'confirm').and.callFake(function () {
      return true;
    });

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('onDeleteCourse method should dispstch state', () => {
    const action = new RemoveCourseAction({id: 1 });

    component.onDeleteCourse(1);

    expect(store.dispatch).toHaveBeenCalledWith(action);
  });

  it('getCourses method should dispatch state', () => {
    const actioen = new GetCoursesAction({ start: '0', count: '10' });

    fixture.detectChanges();
    expect(loadingService.activate).toHaveBeenCalled();
    expect(store.dispatch).toHaveBeenCalledWith(actioen);
  });

  it('loadMore method should call getCourses', () => {
    spyOn(component, 'getCourses');
    component.loadMore();
    expect(component.getCourses).toHaveBeenCalled();
  });

  it('viewAllCourses method should dispatch state and call getCourses method', () => {
    const action = new InitialStateAction;

    spyOn(component, 'getCourses');
    component.viewAllCourses();
    expect(component.getCourses).toHaveBeenCalled();
    expect(store.dispatch).toHaveBeenCalledWith(action);
  });

  it('filterByVideoTitle method should dispatch state', () => {
    const action = new SearchAction({ video: 'video' });
    component.filterByVideoTitle('video');

    expect(loadingService.activate).toHaveBeenCalled();
    expect(store.dispatch).toHaveBeenCalledWith(action);
  });

  it('filterByVideoTitle method should call viewAllCourses method if not search text', () => {
    spyOn(component, 'viewAllCourses');
    component.filterByVideoTitle('');
    expect(component.viewAllCourses).toHaveBeenCalled();
  });

  it('ngOnDestroy method should dispatch new InitialStateAction', () => {
    const action = new InitialStateAction;

    component.ngOnDestroy();
    expect(store.dispatch).toHaveBeenCalledWith(action);
  });
});

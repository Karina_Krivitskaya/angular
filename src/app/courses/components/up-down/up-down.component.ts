import {Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges} from '@angular/core';

@Component({
  selector: 'app-up-down',
  templateUrl: './up-down.component.html',
  styleUrls: ['./up-down.component.css']
})
export class UpDownComponent implements OnInit, OnDestroy, OnChanges {
  @Input() coursesLength: number;
  public showUp: boolean;
  public showDown: boolean;

  constructor() { }

  ngOnInit() {
    window.addEventListener('scroll', this.showUpButton.bind(this));
  }

  public showUpButton() {
    console.log(window);
    const documentElement = document.documentElement;

    const pageY = window.pageYOffset || document.documentElement.scrollTop;
    this.showUp = !this.showDown && pageY > 100;

    if (documentElement.scrollHeight - documentElement.scrollTop === documentElement.clientHeight
      && this.showDown) {
      this.showDown = false;
      this.showUp = true;
    }
  }

  public toggleUpDown() {
    if (this.showUp) {
      window.scrollTo(0, 0);
      this.showUp = false;
      this.showDown = true;
    } else {
      window.scrollTo(0, document.documentElement.scrollHeight);
      this.showDown = false;
      this.showUp = true;
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.coursesLength.currentValue <= 5) {
      this.showUp = false;
      this.showDown = false;
    }
  }

  ngOnDestroy() {
    window.removeEventListener('scroll', this.showUpButton.bind(this));
  }
}

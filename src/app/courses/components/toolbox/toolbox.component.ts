import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {debounceTime, filter} from 'rxjs/internal/operators';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-toolbox',
  templateUrl: './toolbox.component.html',
  styleUrls: ['./toolbox.component.css']
})
export class ToolboxComponent implements OnInit {
  @Output() filterByTitle: EventEmitter<string> = new EventEmitter<string>();
  @Output() viewSortedVideoCourses: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() update: EventEmitter<any> = new EventEmitter();
  public video: string;
  public searchForm: FormControl;

  ngOnInit() {
    this.searchForm = new FormControl();

    this.searchForm.valueChanges.pipe(
      filter(x => !x || x.length >= 3),
      debounceTime(500))
      .subscribe(searchValue => this.handleSearch(searchValue));
  }

  public handleSearch(searchValue: string) {
    this.filterByTitle.emit(searchValue);
  }

  public updateCoursesPage() {
    this.update.emit();
  }
}

import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { ToolboxComponent } from './toolbox.component';
import {RouterTestingModule} from '@angular/router/testing';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {CourseFormComponent} from '../course-form/course-form.component';

describe('ToolboxComponent', () => {
  let component: ToolboxComponent;
  let fixture: ComponentFixture<ToolboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToolboxComponent, CourseFormComponent ],
      imports: [ FormsModule, ReactiveFormsModule, RouterTestingModule.withRoutes([
        { path: 'courses/new', component: CourseFormComponent }
      ]) ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToolboxComponent);
    component = fixture.componentInstance;
    component.video = 'video';
  });

  it('should create', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it('ngOnInit method should call handleSearch with empty string if search value length = 0', fakeAsync(() => {
    spyOn(component, 'handleSearch');
    fixture.detectChanges();
    component.searchForm.setValue('');
    tick(500);
    expect(component.handleSearch).toHaveBeenCalledWith('');
  }));

  it('ngOnInit method should call handleSearch', fakeAsync(() => {
    spyOn(component, 'handleSearch');
    fixture.detectChanges();
    component.searchForm.setValue('video');
    tick(500);
    expect(component.handleSearch).toHaveBeenCalledWith('video');
  }));

  it('ngOnInit method should not call handleSearch if search value length < 3', fakeAsync(() => {
    spyOn(component, 'handleSearch');
    fixture.detectChanges();
    tick(500);
    expect(component.handleSearch).not.toHaveBeenCalled();
  }));

  it('handleSearch method should emit search value', () => {
    fixture.detectChanges();
    component.filterByTitle.subscribe(a => expect(a).toBe('video'));
    component.handleSearch('video');
  });
});

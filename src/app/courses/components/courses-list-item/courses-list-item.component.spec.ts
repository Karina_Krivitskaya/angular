import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CoursesListItemComponent } from './courses-list-item.component';
import {SelectCourseDirective} from '../../directives/select-course/select-course.directive';
import {FormatTimePipe} from '../../pipes/format-time.pipe';
import {Component, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {CourseItem} from '../../models/course-item.model';
import {RouterTestingModule} from '@angular/router/testing';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CourseFormComponent} from '../course-form/course-form.component';

@Component({
  template: `
    <app-courses-list-item
      [videoCourse] = "videoCourse"
      (deleteVideo) = "onDeleteCourse($event)"
    >
  </app-courses-list-item>`
})
class TestHostComponent {
  public id: number;
  public selectCourse: CourseItem;
  public videoCourse = {
    name: 'a',
    id: 1,
    length: 1,
    date: '11.11.2018',
    isTopRated: false,
    description: 'abc',
    authors: []
  };

  public onDeleteCourse(id) { this.id = id; }
}

describe('CoursesListItemComponent', () => {
  let component: TestHostComponent;
  let fixture: ComponentFixture<TestHostComponent>;
  let deleteEl: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        CoursesListItemComponent,
        TestHostComponent,
        SelectCourseDirective,
        FormatTimePipe,
        CourseFormComponent],
      imports: [ FormsModule, ReactiveFormsModule, RouterTestingModule.withRoutes([
        { path: 'courses/:id', component: CourseFormComponent }
      ]) ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    component = fixture.componentInstance;

    deleteEl = fixture.nativeElement.querySelector('#courses-list-item_delete-button');

    component.videoCourse = {
      name: 'a',
      id: 1,
      length: 1,
      date: '11.11.2018',
      isTopRated: false,
      description: 'abc',
      authors: []
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('delete method should emit delete event', () => {
    deleteEl.click();
    expect(component.id).toBe(1);
  });
});

import {AuthorModel} from './author.model';

export interface CourseItemModel {
  name: string;
  id: number;
  length: number;
  date: string;
  isTopRated: boolean;
  description: string;
  authors: any;
}

export interface CourseParams {
  start: string;
  count: string;
}

export interface CourseCountParams {
  count: number;
}

export interface CoursesList {
  coursesList: CourseItem[];
}

export interface CourseId {
  id: number;
}

export interface Course {
  course: CourseItem;
}

export interface CourseVideo {
  video: string;
}

export interface CourseAuthors {
  authors: AuthorModel[];
}

export class CourseItem implements CourseItemModel {
  name: string;
  id: number;
  length: number;
  date: string;
  isTopRated: boolean;
  description: string;
  authors: any;

  public constructor(item: CourseItemModel) {
    this.name = item.name;
    this.id = item.id;
    this.length = item.length;
    this.date = item.date;
    this.isTopRated = item.isTopRated;
    this.description = item.description;
    this.authors = item.authors;
  }
}

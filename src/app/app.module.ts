import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { CoursesModule } from './courses/courses.module';
import { LoginModule } from './login/login.module';
import { AuthService } from './core/services/auth/auth.service';
import {RouterModule, Routes} from '@angular/router';
import {LoginPageComponent} from './login/components/login-page/login-page.component';
import {ErrorPageComponent} from './core/components/error-page/error-page.component';
import {CanActivateGuardService} from './core/services/guards/can-activate-guard.service';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {AuthInterceptor} from './core/services/auth-interceptor/auth-interceptor.service';
import {LoadingService} from './core/services/loading/loading.service';
import {StoreModule} from '@ngrx/store';
import { reducers } from './store/app.state';
import {CoursesEffects} from './courses/effects/courses.effect';
import {EffectsModule} from '@ngrx/effects';
import {AuthEffects} from './core/effects/auth.effect';
import {ReactiveFormsModule} from '@angular/forms';

const appRoutes: Routes = [
  { path: '', redirectTo: '/courses', pathMatch: 'full' },
  { path: 'login', component: LoginPageComponent, pathMatch: 'full' },
  { path: '**', component: ErrorPageComponent }
];

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    CoreModule,
    CoursesModule,
    LoginModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes),
    EffectsModule.forRoot([CoursesEffects, AuthEffects]),
    StoreModule.forRoot(reducers, {}),
  ],
  providers: [AuthService, CanActivateGuardService, LoadingService,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule { }

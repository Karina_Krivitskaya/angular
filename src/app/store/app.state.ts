import * as auth from '../core/reducers/auth.reducer';
import * as courses from '../courses/reducers/courses.reducer';
import {createFeatureSelector} from '@ngrx/store';

export interface AppState {
  authState: auth.State;
  coursesState: courses.State;
}

export const reducers = {
  auth: auth.authReducer,
  courses: courses.coursesReducer
};

export const selectAuthState = createFeatureSelector<AppState>('auth');
export const selectCoursesState = createFeatureSelector<AppState>('courses');

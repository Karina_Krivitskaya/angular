import { AuthService } from './auth.service';

describe('AuthService', () => {
  let authService;
  let httpClientSpy: { get: jasmine.Spy };

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post']);
    authService = new AuthService(<any> httpClientSpy);
  });

  it('should be created', () => {
    expect(authService).toBeTruthy();
  });

  it('login method should to do isAuth is true', () => {
    authService.login('user', 'password');
    expect(authService.http.post).toHaveBeenCalledWith('http://localhost:3004/auth/login', {login: 'user', password: 'password'});
  });

  it('getUserInfo method should return user login', () => {
    authService.getUserInfo();
    expect(authService.http.post).toHaveBeenCalledWith('http://localhost:3004/auth/userinfo', {});
  });
});

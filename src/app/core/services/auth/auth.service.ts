import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {User, UserCredential} from '../../models/user.model';

const BASE_URL = 'http://localhost:3004/auth';

@Injectable()
export class AuthService {
  public userLoginSubject: BehaviorSubject <string> = new BehaviorSubject <string>('');

  constructor(private http: HttpClient) { }

  public login(login: string, password: string): Observable<any>  {
    return this.http.post<UserCredential>(`${BASE_URL}/login`, {login, password});
  }

  public getUserInfo(): Observable<any> {
    return this.http.post<User>(`${BASE_URL}/userinfo`, {});
  }
}

import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {
  public loadingActive: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  constructor() { }

  public activate() {
    this.loadingActive.next(true);
  }

  public deactivate() {
    this.loadingActive.next(false);
  }
}

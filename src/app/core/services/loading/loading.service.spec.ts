import { TestBed, inject } from '@angular/core/testing';

import { LoadingService } from './loading.service';

describe('LoadingService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LoadingService]
    });
  });

  it('should be created', inject([LoadingService], (service: LoadingService) => {
    expect(service).toBeTruthy();
  }));

  it('activate should set next true value', inject([LoadingService], (service: LoadingService) => {
    service.activate();
    service.loadingActive.subscribe(a => expect(a).toBeTruthy());
  }));

  it('activate should set next true value', inject([LoadingService], (service: LoadingService) => {
    service.deactivate();
    service.loadingActive.subscribe(a => expect(a).toBeFalsy());
  }));
});

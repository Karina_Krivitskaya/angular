import { TestBed, inject } from '@angular/core/testing';

import { AuthInterceptor } from './auth-interceptor.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {HTTP_INTERCEPTORS, HttpClient} from '@angular/common/http';

describe('Lang-interceptor.service', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [{
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }]
  }));

  describe('intercept HTTP requests', () => {
    it('should add Authorizatione to Headers if token exist', inject([HttpClient, HttpTestingController],
      (http: HttpClient, mock: HttpTestingController) => {

        localStorage.setItem('user_token', 'abc123');
        http.get('/api').subscribe(response => expect(response).toBeTruthy());
        const request = mock.expectOne(req => (req.headers.has('Authorization') && req.headers.get('Authorization') === 'abc123'));

        request.flush({data: 'test'});
        mock.verify();
      }));
    });

    afterEach(inject([HttpTestingController], (mock: HttpTestingController) => {
      mock.verify();
    }));
});

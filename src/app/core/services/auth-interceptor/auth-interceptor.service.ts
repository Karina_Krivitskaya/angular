import {Injectable} from '@angular/core';
import {HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor() {}

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    const authToken = localStorage.getItem('user_token');

    const authReq = req.clone({
      headers: authToken && req.headers.set('Authorization', authToken)
    });

    return next.handle(authReq);
  }
}

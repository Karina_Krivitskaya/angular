import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable, Subscription} from 'rxjs';
import {Store} from '@ngrx/store';
import {AppState, selectAuthState} from '../../../store/app.state';

@Injectable()
export class CanActivateGuardService implements CanActivate {
  private getState$: Observable<any>;

  constructor(private router: Router,
              private store: Store<AppState>
  ) {
    this.getState$ = this.store.select(selectAuthState);
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.checkLogin();
  }

  public checkLogin() {
    let isAuth;

    this.getState$.subscribe(
      (state) => {
        isAuth = state.isAuthenticated;
      }
    );

    if (isAuth) {
      return Observable.create((observer) => observer.next(true));
    }

    this.router.navigate(['/login']);
    return Observable.create((observer) => observer.next(false));
  }
}

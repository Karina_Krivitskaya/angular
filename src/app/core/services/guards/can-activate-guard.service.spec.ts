import { TestBed } from '@angular/core/testing';

import { CanActivateGuardService } from './can-activate-guard.service';
import {Router} from '@angular/router';
import {Store} from '@ngrx/store';
import {AppState} from '../../../store/app.state';
import {of} from 'rxjs';

class TestStore {
  select() {}
}

describe('CanActivateGuardService', () => {
  let canActivateGuardService: CanActivateGuardService;
  let store: Store<AppState>;
  const router = {
    navigate: jasmine.createSpy('navigate')
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CanActivateGuardService,
        {provide: Router, useValue: router},
        {provide: Store, useClass: TestStore}]
    });
  });

  it('should be created', () => {
    canActivateGuardService = TestBed.get(CanActivateGuardService);
    expect(canActivateGuardService).toBeTruthy();
  });

  it('checkLogin should be true if is authenticated', () => {
    let response;
    const mockData = {
      user: {
        login: 'login',
        token: 'token'
      },
      isAuthenticated: true,
      errorMessage: null
    };
    store = TestBed.get(Store);
    spyOn(store, 'select').and.returnValue(of(mockData));
    canActivateGuardService = TestBed.get(CanActivateGuardService);
    canActivateGuardService.checkLogin().subscribe(data => response = data);
    expect(response).toBeTruthy();
  });

  it('checkLogin should be false if is not authenticated', () => {
    let response;
    const mockData = {
      user: {
        login: 'login',
        token: 'token'
      },
      isAuthenticated: false,
      errorMessage: null
    };
    store = TestBed.get(Store);
    spyOn(store, 'select').and.returnValue(of(mockData));
    canActivateGuardService = TestBed.get(CanActivateGuardService);
    canActivateGuardService.checkLogin().subscribe(data => response = data);
    expect(response).toBeFalsy();
  });

  it('canActivate should call checkLogin method', () => {
    canActivateGuardService.checkLogin = jasmine.createSpy('checkLogin');
    canActivateGuardService.canActivate(null, null);
    expect(canActivateGuardService.checkLogin).toHaveBeenCalled();
  });
});

import {
  AuthActionTypes, LoginAction, LoginFailureAction, LoginSuccessAction, LogoutAction,
  UserInfoAction,
  UserInfoSuccessAction
} from './auth.action';

describe('LoginAction', () => {
  it('should create an action', () => {
    const action = new LoginAction({ login: 'login', password: '123' });

    expect(action.type).toEqual(AuthActionTypes.LOGIN);
  });
});

describe('LogoutAction', () => {
  it('should create an action', () => {
    const action = new LogoutAction;

    expect(action.type).toEqual(AuthActionTypes.LOGOUT);
  });
});

describe('UserInfoAction', () => {
  it('should create an action', () => {
    const action = new UserInfoAction;

    expect(action.type).toEqual(AuthActionTypes.USER_INFO);
  });
});

describe('UserInfoSuccessAction', () => {
  it('should create an action', () => {
    const action = new UserInfoSuccessAction({ login: '' });

    expect(action.type).toEqual(AuthActionTypes.USER_INFO_SUCCESS);
  });
});

describe('LoginSuccessAction', () => {
  it('should create an action', () => {
    const action = new LoginSuccessAction({ token: '' });

    expect(action.type).toEqual(AuthActionTypes.LOGIN_SUCCESS);
  });
});

describe('LoginFailureAction', () => {
  it('should create an action', () => {
    const action = new LoginFailureAction;

    expect(action.type).toEqual(AuthActionTypes.LOGIN_FAILURE);
  });
});

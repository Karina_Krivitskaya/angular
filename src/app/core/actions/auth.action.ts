import {Injectable} from '@angular/core';
import {Action} from '@ngrx/store';
import {UserAccount, UserLogin, UserToken} from '../models/user.model';

export enum AuthActionTypes {
  LOGIN = '[AUTH] LOGIN',
  LOGOUT = '[AUTH] LOGOUT',
  LOGIN_SUCCESS = '[AUTH] LOGIN_SUCCESS',
  LOGIN_FAILURE = '[AUTH] LOGIN_FAILURE',
  USER_INFO = '[AUTH] USER_INFO',
  USER_INFO_SUCCESS = '[AUTH] USER_INFO_SUCCESS',
}

@Injectable()
export class LoginAction implements Action {
  readonly type = AuthActionTypes.LOGIN;
  constructor(public payload: UserAccount) {}
}

export class LogoutAction implements Action {
  readonly type = AuthActionTypes.LOGOUT;
}

export class UserInfoAction implements Action {
  readonly type = AuthActionTypes.USER_INFO;
}

export class UserInfoSuccessAction implements Action {
  readonly type = AuthActionTypes.USER_INFO_SUCCESS;
  constructor(public payload: UserLogin) {}
}

export class LoginSuccessAction implements Action {
  readonly type = AuthActionTypes.LOGIN_SUCCESS;
  constructor(public payload: UserToken) {}
}

export class LoginFailureAction implements Action {
  readonly type = AuthActionTypes.LOGIN_FAILURE;
}

export type AuthActions =
  | LoginAction
  | LogoutAction
  | UserInfoAction
  | UserInfoSuccessAction
  | LoginSuccessAction
  | LoginFailureAction;

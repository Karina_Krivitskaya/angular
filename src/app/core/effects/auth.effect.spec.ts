import {TestBed} from '@angular/core/testing';
import {AuthEffects} from './auth.effect';
import {AuthService} from '../services/auth/auth.service';
import {cold, hot} from 'jasmine-marbles';
import {
  LoginAction, LoginFailureAction, LoginSuccessAction, LogoutAction, UserInfoAction,
  UserInfoSuccessAction
} from '../actions/auth.action';
import {Store} from '@ngrx/store';
import {Observable, of, throwError} from 'rxjs';
import {LoadingService} from '../services/loading/loading.service';
import {Router} from '@angular/router';
import {provideMockActions} from '@ngrx/effects/testing';

class TestStore {
  constructor(private state: any) {
    this.state = state;
  }
  dispatch = () => undefined;
  select = () => of(this.state);
}

describe('AuthEffects', () => {
  let authEfects: AuthEffects;
  let loadingService;
  let actions: Observable<any>;
  let authService;
  let router;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AuthEffects,
        {
          provide: AuthService,
          useValue: {
            login: () => of({}),
            getUserInfo: () => of({}),
            userLoginSubject: {
              next: () => of({})
            }
          }
        },
        {
          provide: LoadingService,
          useValue: {
            deactivate: () => {}
          }
        },
        {
          provide: Store,
          useValue: new TestStore({
            isAuthenticated: true,
            user: {
              login: '',
              token: ''
            },
            errorMessage: null
          })
        },
        {
          provide: Router,
          useValue: {
            navigate: () => {}
          }
        },
        provideMockActions(() => actions)
      ]
    });
    authEfects = TestBed.get(AuthEffects);
    router = TestBed.get(Router);
    authService = TestBed.get(AuthService);
    loadingService = TestBed.get(LoadingService);

    spyOn(loadingService, 'deactivate');
    spyOn(router, 'navigate');
  });

  it('should be created', () => {
    expect(authEfects).toBeTruthy();
  });

  describe('authLogin', () => {
    it('should return an LoginSuccessAction action on success', () => {
      const action = new LoginAction({ login: 'login', password: '123' });
      const completion = new LoginSuccessAction({token: '123abc'} );

      actions = hot('a', { a: action });
      const expected = cold('b', { b: completion });

      spyOn(authService, 'login').and.returnValue(of({token: '123abc'}));

      expect(authEfects.authLogin).toBeObservable(expected);
    });

    it('should return an LoginFailureAction action, with an error, on failure', () => {
      const action = new LoginAction({ login: 'login', password: '123' });
      const completion = new LoginFailureAction;

      actions = hot('a', { a: action });
      const expected = cold('b', { b: completion });

      spyOn(authService, 'login').and.returnValue(throwError({}));

      expect(authEfects.authLogin).toBeObservable(expected);
    });
  });

  describe('authLoginSuccess', () => {
    it('should navigate to courses page', () => {
      const action = new LoginSuccessAction({ token: '123abc' });

      actions = of(action);
      authEfects.authLoginSuccess.subscribe();

      expect(router.navigate).toHaveBeenCalledWith(['/courses']);
      expect(localStorage.getItem('user_token')).toEqual('123abc');
    });
  });

  describe('LogInFailure', () => {
    it('should deactivate loading', () => {
      const action = new LoginFailureAction;

      actions = of(action);
      authEfects.LogInFailure.subscribe();

      expect(loadingService.deactivate).toHaveBeenCalled();
    });
  });

  describe('authUserInfo', () => {
    it('should return an UserInfoSuccess action on success', () => {
      const action = new UserInfoAction;
      const completion = new UserInfoSuccessAction({login: 'login'} );

      actions = hot('a', { a: action });
      const expected = cold('b', { b: completion });

      spyOn(authService, 'getUserInfo').and.returnValue(of({login: 'login'}));

      expect(authEfects.authUserInfo).toBeObservable(expected);
    });
  });

  describe('authUserInfoSuccess', () => {
    it('should set user login in local storage', () => {
      const action = new UserInfoSuccessAction({ login: 'login' });

      actions = of(action);
      authEfects.authUserInfoSuccess.subscribe();

      expect(localStorage.getItem('user_login')).toEqual('login');
    });
  });

  describe('authUserInfoSuccess', () => {
    it('should set user login in local storage', () => {
      const action = new UserInfoSuccessAction({ login: 'login' });

      actions = of(action);
      authEfects.authUserInfoSuccess.subscribe();

      expect(localStorage.getItem('user_login')).toEqual('login');
    });
  });

  describe('logout', () => {
    it('should navigate to login page', () => {
      const action = new LogoutAction;

      actions = of(action);
      authEfects.logout.subscribe();

      expect(router.navigate).toHaveBeenCalledWith(['/login']);
      expect(localStorage.getItem('user_login')).toEqual(null);
    });
  });
});

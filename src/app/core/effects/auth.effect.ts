import {Injectable} from '@angular/core';
import {Action, Store} from '@ngrx/store';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {catchError, map, switchMap, tap} from 'rxjs/internal/operators';
import {AuthService} from '../services/auth/auth.service';
import {
  AuthActionTypes, LoginAction, LoginFailureAction, LoginSuccessAction, UserInfoAction, UserInfoSuccessAction
} from '../actions/auth.action';
import {Observable, of} from 'rxjs';
import {Router} from '@angular/router';
import {LoadingService} from '../services/loading/loading.service';

@Injectable()
export class AuthEffects {

  constructor(
    private store: Store<any>,
    private actions: Actions,
    private authService: AuthService,
    private router: Router,
    private loadingService: LoadingService
  ) {}

  @Effect()
  authLogin: Observable<Action> = this.actions.pipe(
    ofType(AuthActionTypes.LOGIN),
    map((action: LoginAction) => action.payload),
    switchMap(payload => {
      return this.authService.login(payload.login, payload.password).pipe(
        map(user => new LoginSuccessAction({ token: user.token })),
        catchError(error => of(new LoginFailureAction))
      );
    })
  );

  @Effect({ dispatch: false })
  authLoginSuccess: Observable<any> = this.actions.pipe(
    ofType(AuthActionTypes.LOGIN_SUCCESS),
    tap((user) => {
      this.store.dispatch(new UserInfoAction);
      localStorage.setItem('user_token', user.payload.token);
      this.router.navigate(['/courses']);
      this.loadingService.deactivate();
    })
  );

  @Effect({ dispatch: false })
  LogInFailure: Observable<any> = this.actions.pipe(
    ofType(AuthActionTypes.LOGIN_FAILURE),
    tap(() => {
      this.loadingService.deactivate();
    })
  );

  @Effect({ dispatch: false })
  logout: Observable<any> = this.actions.pipe(
    ofType(AuthActionTypes.LOGOUT),
    tap(() => {
      localStorage.removeItem('user_token');
      localStorage.removeItem('user_login');
      this.authService.userLoginSubject.next(null);
      this.router.navigate(['/login']);
    })
  );

  @Effect()
  authUserInfo: Observable<Action> = this.actions.pipe(
    ofType(AuthActionTypes.USER_INFO),
    switchMap(() => {
      return this.authService.getUserInfo().pipe(
        map(user => new UserInfoSuccessAction({ login: user.login }))
      );
    })
  );

  @Effect({ dispatch: false })
  authUserInfoSuccess: Observable<any> = this.actions.pipe(
    ofType(AuthActionTypes.USER_INFO_SUCCESS),
    tap((user) => {
      localStorage.setItem('user_login', user.payload.login);
    })
  );
}

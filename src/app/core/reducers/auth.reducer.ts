import {AuthActions, AuthActionTypes} from '../actions/auth.action';

import {UserCredential} from '../models/user.model';

export interface State {
  isAuthenticated: boolean;
  user: UserCredential;
  errorMessage: string;
}

export const initialState: State = {
  isAuthenticated: !!localStorage.getItem('user_token'),
  user: {
    login: localStorage.getItem('user_login'),
    token: localStorage.getItem('user_token')
  },
  errorMessage: null
};

export function authReducer(state = initialState, action: AuthActions) {
  switch (action.type) {
    case AuthActionTypes.LOGIN_SUCCESS: {
      return {
        ...state,
        isAuthenticated: true,
        user: {
          token: action.payload.token
        },
        errorMessage: null
      };
    }
    case AuthActionTypes.LOGIN_FAILURE: {
      return {
        ...state,
        errorMessage: 'Incorrect email and/or password.'
      };
    }
    case AuthActionTypes.USER_INFO_SUCCESS: {
      return {
        ...state,
        user: {
          login: action.payload.login
        }
      };
    }
    case AuthActionTypes.LOGOUT: {
      return {
        ...state,
        isAuthenticated: false,
        user: {
          token: null,
          login: null
        },
        errorMessage: null
      };
    }
    default: {
      return state;
    }
  }
}

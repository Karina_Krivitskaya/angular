import * as fromAuth from './auth.reducer';
import {LoginAction, LoginFailureAction, LoginSuccessAction, LogoutAction,
  UserInfoSuccessAction
} from '../actions/auth.action';

describe('authReducer', () => {
  describe('undefined action', () => {
    it('should return the default state', () => {
      const { initialState } = fromAuth;
      const state = fromAuth.authReducer(undefined, new LoginAction({}));

      expect(state).toBe(initialState);
    });
  });

  describe('LoginSuccess action', () => {
    it('should return correct state', () => {
      const { initialState } = fromAuth;
      const state = fromAuth.authReducer(initialState, new LoginSuccessAction({ token: 'abc' }));

      expect(state.isAuthenticated).toBeTruthy();
      expect(state.user.token).toEqual('abc');
    });
  });

  describe('LoginFailure action', () => {
    it('should return correct state', () => {
      const { initialState } = fromAuth;
      const state = fromAuth.authReducer(initialState, new LoginFailureAction);

      expect(state.errorMessage).toEqual('Incorrect email and/or password.');
    });
  });

  describe('UserInfosuccess action', () => {
    it('should return correct state', () => {
      const { initialState } = fromAuth;
      const state = fromAuth.authReducer(initialState, new UserInfoSuccessAction({ login: 'login' }));

      expect(state.user.login).toEqual('login');
    });
  });

  describe('Logout action', () => {
    it('should return correct state', () => {
      const { initialState } = fromAuth;
      const state = fromAuth.authReducer(initialState, new LogoutAction);

      expect(state.isAuthenticated).toBeFalsy();
    });
  });
});

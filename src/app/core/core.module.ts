import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { ErrorPageComponent } from './components/error-page/error-page.component';
import {HttpClientModule} from '@angular/common/http';
import { LoadingComponent } from './components/loading/loading.component';
import { SpinnerComponent } from './components/loading/spinner/spinner.component';


@NgModule({
  imports: [
    CommonModule,
    HttpClientModule
  ],
  declarations: [
    HeaderComponent,
    FooterComponent,
    ErrorPageComponent,
    LoadingComponent,
    SpinnerComponent],
  exports: [
    HeaderComponent,
    FooterComponent,
    ErrorPageComponent,
    LoadingComponent]
})
export class CoreModule { }

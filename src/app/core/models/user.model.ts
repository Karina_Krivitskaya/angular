export interface User {
  id: number;
  fakeToken: string;
  name: {
    first: string;
    last: string;
  };
  login: string;
  password: string;
}

export interface UserCredential {
  login: string;
  token: string;
}

export interface UserAccount {
  login: string;
  password: string;
}

export interface UserLogin {
  login: string;
}

export interface UserToken {
  token: string;
}

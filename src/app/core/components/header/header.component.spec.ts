import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderComponent } from './header.component';
import {of} from 'rxjs';
import {AppState} from '../../../store/app.state';
import {Store} from '@ngrx/store';
import {LoginAction, LogoutAction} from '../../actions/auth.action';

class TestStore {
  select() {}
  dispatch() {}
}

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;
  let store: Store<AppState>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderComponent ],
      providers: [{provide: Store, useClass: TestStore}]
    })
    .compileComponents();
    store = TestBed.get(Store);
    const mockData = {
      user: {
        login: 'login',
        token: 'token'
      },
      isAuthenticated: true,
      errorMessage: null
    };
    spyOn(store, 'dispatch').and.callThrough();
    spyOn(store, 'select').and.returnValue(of(mockData));
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnInit method should get state', () => {
    component.ngOnInit();
    expect(component.login).toEqual('login');
    expect(component.isAuthenticated).toBeTruthy();
  });

  it('logoff method should call logout method from service', () => {
    const action = new LogoutAction;
    component.logoff();
    expect(store.dispatch).toHaveBeenCalledWith(action);
  });
});

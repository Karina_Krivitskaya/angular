import {Component, OnDestroy, OnInit} from '@angular/core';
import {AppState, selectAuthState} from '../../../store/app.state';
import {Store} from '@ngrx/store';
import {LogoutAction} from '../../actions/auth.action';
import {Observable, Subscription} from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {
  public login: string;
  public isAuthenticated: boolean;
  private getStateSubscription$: Subscription;
  public getState$: Observable<any>;

  constructor (private store: Store<AppState>) {
    this.getState$ = this.store.select(selectAuthState);
  }

  ngOnInit() {
    this.getStateSubscription$ = this.getState$.subscribe(state => {
      this.login = state.user.login;
      this.isAuthenticated = state.isAuthenticated;
    });
  }

  public logoff() {
    this.store.dispatch(new LogoutAction);
  }

  ngOnDestroy() {
    this.getStateSubscription$.unsubscribe();
  }
}

import { Component, OnInit } from '@angular/core';
import {LoadingService} from '../../services/loading/loading.service';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.css']
})
export class LoadingComponent implements OnInit {
  public isLoading: boolean;
  constructor(private loadingService: LoadingService) { }

  ngOnInit() {
    this.loadingService.loadingActive.subscribe(isLoading => this.isLoading = isLoading);
  }
}

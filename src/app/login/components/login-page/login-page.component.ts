import {Component, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subscription} from 'rxjs';
import {LoadingService} from '../../../core/services/loading/loading.service';
import {Store} from '@ngrx/store';
import {AppState, selectAuthState} from '../../../store/app.state';
import {LoginAction} from '../../../core/actions/auth.action';
import {FormBuilder, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit, OnDestroy {
  public userForm: FormGroup;
  public errorText: string;
  private getStateSubscription$: Subscription;
  public getState$: Observable<any>;

  constructor(private store: Store<AppState>,
              private fb: FormBuilder,
              private loadingService: LoadingService) {
    this.getState$ = this.store.select(selectAuthState);
  }

  ngOnInit() {
    this.userForm = this.fb.group({
      login: [''],
      password: ['']
    });

    this.getStateSubscription$ = this.getState$.subscribe(
    (state) => {
      this.errorText = state.errorMessage;
    });
  }

  public login() {
    this.loadingService.activate();
    this.store.dispatch(new LoginAction({
      login: this.userForm.get('login').value,
      password: this.userForm.get('password') .value
    }));
  }

  ngOnDestroy() {
    this.getStateSubscription$.unsubscribe();
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginPageComponent } from './login-page.component';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {FormBuilder, FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CoursesPageComponent} from '../../../courses/components/courses-page/courses-page.component';
import {LoadingService} from '../../../core/services/loading/loading.service';
import {Store} from '@ngrx/store';
import {AppState} from '../../../store/app.state';
import {of} from 'rxjs';
import {LoginAction} from '../../../core/actions/auth.action';
import {BrowserModule} from '@angular/platform-browser';

class TestStore {
  select() {}
  dispatch() {}
}

describe('LoginPageComponent', () => {
  let component: LoginPageComponent;
  let fixture: ComponentFixture<LoginPageComponent>;
  let loadingService: Partial<LoadingService>;
  let store: Store<AppState>;
  let fb: FormBuilder;

  beforeEach(async(() => {
    const spy = {
      activate: () => {}
    };
    TestBed.configureTestingModule({
      imports: [FormsModule, BrowserModule, ReactiveFormsModule],
      declarations: [ LoginPageComponent, CoursesPageComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [FormBuilder, { provide: LoadingService, useValue: spy },
        {provide: Store, useClass: TestStore}]
    })
    .compileComponents();
    loadingService = TestBed.get(LoadingService);
    fb = TestBed.get(FormBuilder);
    spyOn(loadingService, 'activate');
    store = TestBed.get(Store);
    const mockData = {
      user: {
        login: 'login',
        token: 'token'
      },
      isAuthenticated: true,
      errorMessage: null
    };

    spyOn(store, 'dispatch').and.callThrough();
    spyOn(store, 'select').and.returnValue(of(mockData));
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginPageComponent);
    component = fixture.componentInstance;
    component.userForm = fb.group({
      login: [''],
      password: ['']
    });
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('login method should call login method from service', () => {
    const action = new LoginAction({ login: 'login', password: 'password' });
    component.userForm.controls['login'].setValue('login');
    component.userForm.controls['password'].setValue('password');
    component.login();
    expect(store.dispatch).toHaveBeenCalledWith(action);
  });
});
